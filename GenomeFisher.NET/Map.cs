﻿using System.Collections.Generic;

namespace GenomeFisher.NET
{
    class Map<TK, TV> : Dictionary<TK, TV>
    {
        private List<TK> _mLKeys;
        public List<TK> KeyList
        {
            get
            {
                if ((_mLKeys == null) || (_mLKeys.Count != Keys.Count))
                {
                    _mLKeys = new List<TK>(Keys);
                }
                return _mLKeys;
            }
        }

        public Map()
        {
            _mLKeys = null;
        }
    }
}
