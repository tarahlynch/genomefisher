﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace GenomeFisher.NET
{
    public class CompoundMarker
    {
        private readonly List<bool> _markerTypes = new List<bool>();
        private readonly List<SingleMarker> _singleMarkers;
        private readonly List<BitArray> _strainsInGroupA = new List<BitArray>();
        private readonly List<BitArray> _strainsInGroupB = new List<BitArray>();
        private int _a;
        private int _b;
        private string _binaryDistribution;
        private int _c;
        private ContingencyTable _contingencyTable;
        private int _d;
        private double _pValue = -1;
        private double _rateAB;
        private double _rateCD;
        private double _rateDifference;
        private double _sensitivity;
        private double _specificity;
        private double _specificity2;

        public CompoundMarker(List<SingleMarker> singleMarkers)
        {
            _singleMarkers = singleMarkers;
            GetMarkerInfo();
            GetStatistics();
        }

        public CompoundMarker(SingleMarker marker)
        {
            _singleMarkers = new List<SingleMarker> {marker};
            GetMarkerInfo();
            //get stats from SingleMarker obj
            _a = marker.A;
            _b = marker.B;
            _c = marker.C;
            _d = marker.D;
            _sensitivity = marker.Sensitivity;
            _specificity = marker.Specificity;
            _specificity2 = marker.Specificity2;
            _rateAB = marker.RateAB;
            _rateCD = marker.RateCD;
            _rateDifference = marker.RateDifference;
            _contingencyTable = marker.ContingencyTable;
            _pValue = marker.PValue;
        }

        public int MarkerCount { get { return _singleMarkers.Count; } }
        public List<BitArray> StrainsInGroupA { get { return _strainsInGroupA; } }

        public List<BitArray> StrainsInGroupB { get { return _strainsInGroupB; } }

        public IEnumerable<bool> MarkerTypes { get { return _markerTypes; } }

        public string MarkerTypesString
        {
            get
            {
                var list = new List<string>();
                foreach (bool markerType in _markerTypes)
                {
                    list.Add(markerType ? "+" : "-");
                }
                return string.Join(";", list);
            }
        }

        public double Sensitivity { get { return _sensitivity; } }

        public double Specificity { get { return _specificity; } }

        public double Specificity2 { get { return _specificity2; } }

        public ContingencyTable ContingencyTable { get { return _contingencyTable; } }

        public double PValue
        {
            get
            {
                if (Math.Abs(_pValue - -1) < double.Epsilon)
                    _pValue = _contingencyTable.GetFisher2TailPermutationTest();
                return _pValue;
            }
        }

        public int A { get { return _a; } }

        public int B { get { return _b; } }

        public int C { get { return _c; } }

        public int D { get { return _d; } }

        public string BinaryDistribution { get { return _binaryDistribution; } }

        public double RateAB { get { return _rateAB; } }

        public double RateCD { get { return _rateCD; } }

        public double RateDifference { get { return _rateDifference; } }

        public List<SingleMarker> SingleMarkers { get { return _singleMarkers; } }

        public double AverageImprovementOverSingleMarkers
        {
            get
            {
                double avg = 0;
                int count = 0;
                foreach (SingleMarker marker in _singleMarkers)
                {
                    avg += marker.RateDifference;
                    count++;
                }
                avg = avg / count;
                return _rateDifference - avg;
            }
        }

        private void GetMarkerInfo()
        {
            foreach (SingleMarker m in _singleMarkers)
            {
                _markerTypes.Add(m.MarkerType);
                _strainsInGroupA.Add(m.GroupAStrainDistr);
                _strainsInGroupB.Add(m.GroupBStrainDistr);
            }
        }

        private void GetStatistics()
        {
            var dict = new Dictionary<string, int>();
            var dict2 = new Dictionary<string, int>();

            var sb = new StringBuilder();
            
            for (int i = 0; i < _strainsInGroupA[0].Length; i++)
            {
                sb.Clear();
                for (int j = 0; j < _singleMarkers.Count; j++)
                {
                    sb.Append(_strainsInGroupA[j][i] ? "1" : "0");
                }

                string s = sb.ToString();

                if (dict.ContainsKey(s))
                {
                    dict[s]++;
                }
                else
                {
                    dict.Add(s, 1);
                    dict2.Add(s, 0);
                }
            }
            for (int i = 0; i < _strainsInGroupB[0].Length; i++)
            {
                sb.Clear();
                for (int j = 0; j < _singleMarkers.Count; j++)
                {
                    sb.Append(_strainsInGroupB[j][i] ? "1" : "0");
                }

                string s = sb.ToString();

                if (dict2.ContainsKey(s))
                {
                    dict2[s]++;
                }
                else
                {
                    dict2.Add(s, 1);
                }
            }

            int sum1 = _strainsInGroupA[0].Length;
            int sum2 = _strainsInGroupB[0].Length;
            PatternSearch(dict, dict2,sum1, sum2, ref _binaryDistribution, ref _rateAB, ref _rateCD, out _rateDifference, ref _a, ref _c);

            _b = sum1 - _a;
            //what is the number of group B samples that have the same binary pattern as the "present" group A
            //number of group B samples that don't have the same binary pattern as the "present" group A
            _d = sum2 - _c;

            if (_a > _b)
            {
                _sensitivity = _a / (double) sum1;
                _specificity = _d / (double) sum2;
            }
            else
            {
                _sensitivity = _b / (double) sum1;
                _specificity = _c / (double) sum2;
            }


            _specificity2 = _a / (double) (_a + _c);

            _contingencyTable = new ContingencyTable(_a, _b, _c, _d, -1, null);
            //calculate p-value when it's needed
            //_pValue = _contingencyTable.GetFisher2TailPermutationTest();
        }

        public string[] GetBinaryDistr()
        {
            var tmp = new string[_strainsInGroupA[0].Length + _strainsInGroupB[0].Length];
            int count = 0;
            var sb = new StringBuilder();
            for (int i = 0; i < _strainsInGroupA[0].Length; i++)
            {
                sb.Clear();
                for (int j = 0; j < _singleMarkers.Count; j++)
                {
                    sb.Append(_strainsInGroupA[j][i] ? "1" : "0");
                }
                tmp[count++] = (sb.ToString() == _binaryDistribution ? "1" : "0");
            }
            for (int i = 0; i < _strainsInGroupB[0].Length; i++)
            {
                sb.Clear();
                for (int j = 0; j < _singleMarkers.Count; j++)
                {
                    sb.Append(_strainsInGroupB[j][i] ? "1" : "0");
                }
                tmp[count++] = (sb.ToString() == _binaryDistribution ? "1" : "0");
            }

            return tmp;
        }


        private void PatternSearch(Dictionary<string, int> dict1,
                                   Dictionary<string, int> dict2,
                                   int sumDict1,
                                   int sumDict2,
                                   ref string bestPattern,
                                   ref double bestRateAB,
                                   ref double bestRateCD,
                                   out double bestRateDiff,
                                   ref int bestA,
                                   ref int bestC)
        {
            var binaryStrings = new HashSet<string>();

            foreach (var pair in dict1)
            {
                binaryStrings.Add(pair.Key);
            }
            foreach (var pair in dict2)
            {
                binaryStrings.Add(pair.Key);
            }

            bestRateDiff = double.MinValue;
            foreach (string binaryString in binaryStrings)
            {
                int a;
                if (!dict1.TryGetValue(binaryString, out a))
                    a = 0;
                int c;
                if (!dict2.TryGetValue(binaryString, out c))
                    c = 0;
                double rateAB = a / (double) sumDict1;
                double rateCD = c / (double) sumDict2;
                double rateDiff = Math.Abs(rateAB - rateCD);
                if (rateDiff <= bestRateDiff)
                    continue;
                bestRateDiff = rateDiff;
                bestPattern = binaryString;
                bestA = a;
                bestC = c;
                bestRateAB = rateAB;
                bestRateCD = rateCD;
            }
        }
    }
}