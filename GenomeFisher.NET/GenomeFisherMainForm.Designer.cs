﻿namespace GenomeFisher.NET
{
    partial class GenomeFisherMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GenomeFisherMainForm));
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.olvFETGroups = new BrightIdeasSoftware.FastObjectListView();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.gbxGroupSelection = new System.Windows.Forms.GroupBox();
            this.lvwGroupSelection = new GenomeFisher.NET.ListViewFF();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenuGroupSelection = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnCreateNewGroup = new System.Windows.Forms.ToolStripMenuItem();
            this.btnCreateNewGroupIntoNewGroupCollection = new System.Windows.Forms.ToolStripMenuItem();
            this.btnClearSelection = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.splitContainer5 = new System.Windows.Forms.SplitContainer();
            this.gbxGroupCollection = new System.Windows.Forms.GroupBox();
            this.olvGroupCollections = new BrightIdeasSoftware.FastObjectListView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.olvGroups = new BrightIdeasSoftware.FastObjectListView();
            this.gbxFET = new System.Windows.Forms.GroupBox();
            this.olvFETResults = new BrightIdeasSoftware.FastObjectListView();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbFile = new System.Windows.Forms.ToolStripDropDownButton();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnOpenAnalysis = new System.Windows.Forms.ToolStripMenuItem();
            this.btnOpenDataFiles = new System.Windows.Forms.ToolStripMenuItem();
            this.numericDataFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sampleInfoFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnOpenThresholdsFile = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSave = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsbPreferences = new System.Windows.Forms.ToolStripDropDownButton();
            this.btnShowStatusLog = new System.Windows.Forms.ToolStripMenuItem();
            this.btnShowSigFETOnly = new System.Windows.Forms.ToolStripMenuItem();
            this.thresholdToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtThreshold = new System.Windows.Forms.ToolStripTextBox();
            this.minGroupSizeForAutomatedGroupCreationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtMinSizeGroup = new System.Windows.Forms.ToolStripTextBox();
            this.pvalueThresholdToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtPvalueThreshold = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsbGroupCreation = new System.Windows.Forms.ToolStripDropDownButton();
            this.btnAutoGroupCreation = new System.Windows.Forms.ToolStripMenuItem();
            this.btnCompoundGroupCreation = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuGroupCollection = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnChangeGroupCollectionName = new System.Windows.Forms.ToolStripMenuItem();
            this.btnAddNewGroupCollection = new System.Windows.Forms.ToolStripMenuItem();
            this.btnRemoveGroupCollection = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnCalcAllPairwiseComparisons = new System.Windows.Forms.ToolStripMenuItem();
            this.btnCalcAllNotInGroupComparisons = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuGroups = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnChangeGroupName = new System.Windows.Forms.ToolStripMenuItem();
            this.btnRemovewGroup = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.btnCalcPairwiseComparisons = new System.Windows.Forms.ToolStripMenuItem();
            this.btnCalcNotInGroupComparisons = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.getAllSubgroupsForCategoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cbxCategories = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.showGroupInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuFETGroups = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.removeSelectedFETComparisonsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.calculateFDRToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnDetermineCompoundMarkers = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuFETResults = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnSaveFETResults = new System.Windows.Forms.ToolStripMenuItem();
            this.bgw = new System.ComponentModel.BackgroundWorker();
            this.toolStripContainer1.BottomToolStripPanel.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.olvFETGroups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            this.gbxGroupSelection.SuspendLayout();
            this.contextMenuGroupSelection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).BeginInit();
            this.splitContainer5.Panel1.SuspendLayout();
            this.splitContainer5.Panel2.SuspendLayout();
            this.splitContainer5.SuspendLayout();
            this.gbxGroupCollection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.olvGroupCollections)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.olvGroups)).BeginInit();
            this.gbxFET.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.olvFETResults)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.contextMenuGroupCollection.SuspendLayout();
            this.contextMenuGroups.SuspendLayout();
            this.contextMenuFETGroups.SuspendLayout();
            this.contextMenuFETResults.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.BottomToolStripPanel
            // 
            this.toolStripContainer1.BottomToolStripPanel.Controls.Add(this.statusStrip1);
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.splitContainer1);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(1245, 515);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(1245, 562);
            this.toolStripContainer1.TabIndex = 0;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip1);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 0);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1245, 22);
            this.statusStrip1.TabIndex = 0;
            // 
            // tslStatus
            // 
            this.tslStatus.Name = "tslStatus";
            this.tslStatus.Size = new System.Drawing.Size(39, 17);
            this.tslStatus.Text = "Ready";
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer4);
            this.splitContainer1.Size = new System.Drawing.Size(1245, 515);
            this.splitContainer1.SplitterDistance = 251;
            this.splitContainer1.TabIndex = 0;
            // 
            // splitContainer2
            // 
            this.splitContainer2.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.AutoScroll = true;
            this.splitContainer2.Panel1.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer2.Panel1.SizeChanged += new System.EventHandler(this.SplitContainer2Panel1SizeChanged);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.groupBox3);
            this.splitContainer2.Size = new System.Drawing.Size(1245, 251);
            this.splitContainer2.SplitterDistance = 700;
            this.splitContainer2.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.olvFETGroups);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(537, 247);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "FET Comparisons";
            // 
            // olvFETGroups
            // 
            this.olvFETGroups.CheckBoxes = false;
            this.olvFETGroups.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olvFETGroups.Location = new System.Drawing.Point(3, 16);
            this.olvFETGroups.Name = "olvFETGroups";
            this.olvFETGroups.ShowGroups = false;
            this.olvFETGroups.Size = new System.Drawing.Size(531, 228);
            this.olvFETGroups.TabIndex = 1;
            this.olvFETGroups.UseCompatibleStateImageBehavior = false;
            this.olvFETGroups.View = System.Windows.Forms.View.Details;
            this.olvFETGroups.VirtualMode = true;
            // 
            // splitContainer4
            // 
            this.splitContainer4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.gbxGroupSelection);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer4.Size = new System.Drawing.Size(1245, 260);
            this.splitContainer4.SplitterDistance = 311;
            this.splitContainer4.TabIndex = 0;
            // 
            // gbxGroupSelection
            // 
            this.gbxGroupSelection.Controls.Add(this.lvwGroupSelection);
            this.gbxGroupSelection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbxGroupSelection.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxGroupSelection.Location = new System.Drawing.Point(0, 0);
            this.gbxGroupSelection.Name = "gbxGroupSelection";
            this.gbxGroupSelection.Size = new System.Drawing.Size(307, 256);
            this.gbxGroupSelection.TabIndex = 1;
            this.gbxGroupSelection.TabStop = false;
            this.gbxGroupSelection.Text = "Group Item Selection";
            // 
            // lvwGroupSelection
            // 
            this.lvwGroupSelection.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.lvwGroupSelection.ContextMenuStrip = this.contextMenuGroupSelection;
            this.lvwGroupSelection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvwGroupSelection.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lvwGroupSelection.FullRowSelect = true;
            this.lvwGroupSelection.GridLines = true;
            this.lvwGroupSelection.Location = new System.Drawing.Point(3, 16);
            this.lvwGroupSelection.Name = "lvwGroupSelection";
            this.lvwGroupSelection.ShowItemToolTips = true;
            this.lvwGroupSelection.Size = new System.Drawing.Size(301, 237);
            this.lvwGroupSelection.TabIndex = 0;
            this.lvwGroupSelection.UseCompatibleStateImageBehavior = false;
            this.lvwGroupSelection.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Category";
            this.columnHeader1.Width = 100;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Subgroup";
            this.columnHeader2.Width = 78;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Count";
            this.columnHeader3.Width = 40;
            // 
            // contextMenuGroupSelection
            // 
            this.contextMenuGroupSelection.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnCreateNewGroup,
            this.btnCreateNewGroupIntoNewGroupCollection,
            this.btnClearSelection});
            this.contextMenuGroupSelection.Name = "contextMenuGroupSelection";
            this.contextMenuGroupSelection.Size = new System.Drawing.Size(332, 70);
            // 
            // btnCreateNewGroup
            // 
            this.btnCreateNewGroup.Name = "btnCreateNewGroup";
            this.btnCreateNewGroup.Size = new System.Drawing.Size(331, 22);
            this.btnCreateNewGroup.Text = "Create New Group Into Current Group Collection";
            // 
            // btnCreateNewGroupIntoNewGroupCollection
            // 
            this.btnCreateNewGroupIntoNewGroupCollection.Name = "btnCreateNewGroupIntoNewGroupCollection";
            this.btnCreateNewGroupIntoNewGroupCollection.Size = new System.Drawing.Size(331, 22);
            this.btnCreateNewGroupIntoNewGroupCollection.Text = "Create New Group Into New Group Collection";
            // 
            // btnClearSelection
            // 
            this.btnClearSelection.Name = "btnClearSelection";
            this.btnClearSelection.Size = new System.Drawing.Size(331, 22);
            this.btnClearSelection.Text = "Clear Selection";
            // 
            // splitContainer3
            // 
            this.splitContainer3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.splitContainer5);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.gbxFET);
            this.splitContainer3.Size = new System.Drawing.Size(930, 260);
            this.splitContainer3.SplitterDistance = 476;
            this.splitContainer3.TabIndex = 0;
            // 
            // splitContainer5
            // 
            this.splitContainer5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer5.Location = new System.Drawing.Point(0, 0);
            this.splitContainer5.Name = "splitContainer5";
            // 
            // splitContainer5.Panel1
            // 
            this.splitContainer5.Panel1.Controls.Add(this.gbxGroupCollection);
            // 
            // splitContainer5.Panel2
            // 
            this.splitContainer5.Panel2.Controls.Add(this.groupBox2);
            this.splitContainer5.Size = new System.Drawing.Size(476, 260);
            this.splitContainer5.SplitterDistance = 158;
            this.splitContainer5.TabIndex = 0;
            // 
            // gbxGroupCollection
            // 
            this.gbxGroupCollection.Controls.Add(this.olvGroupCollections);
            this.gbxGroupCollection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbxGroupCollection.Location = new System.Drawing.Point(0, 0);
            this.gbxGroupCollection.Name = "gbxGroupCollection";
            this.gbxGroupCollection.Size = new System.Drawing.Size(154, 256);
            this.gbxGroupCollection.TabIndex = 0;
            this.gbxGroupCollection.TabStop = false;
            this.gbxGroupCollection.Text = "Group Collection";
            // 
            // olvGroupCollections
            // 
            this.olvGroupCollections.CheckBoxes = false;
            this.olvGroupCollections.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olvGroupCollections.Location = new System.Drawing.Point(3, 16);
            this.olvGroupCollections.Name = "olvGroupCollections";
            this.olvGroupCollections.ShowGroups = false;
            this.olvGroupCollections.Size = new System.Drawing.Size(148, 237);
            this.olvGroupCollections.TabIndex = 0;
            this.olvGroupCollections.UseCompatibleStateImageBehavior = false;
            this.olvGroupCollections.View = System.Windows.Forms.View.Details;
            this.olvGroupCollections.VirtualMode = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.olvGroups);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(310, 256);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Groups";
            // 
            // olvGroups
            // 
            this.olvGroups.CheckBoxes = false;
            this.olvGroups.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olvGroups.Location = new System.Drawing.Point(3, 16);
            this.olvGroups.Name = "olvGroups";
            this.olvGroups.ShowGroups = false;
            this.olvGroups.Size = new System.Drawing.Size(304, 237);
            this.olvGroups.TabIndex = 0;
            this.olvGroups.UseCompatibleStateImageBehavior = false;
            this.olvGroups.View = System.Windows.Forms.View.Details;
            this.olvGroups.VirtualMode = true;
            // 
            // gbxFET
            // 
            this.gbxFET.Controls.Add(this.olvFETResults);
            this.gbxFET.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbxFET.Location = new System.Drawing.Point(0, 0);
            this.gbxFET.Name = "gbxFET";
            this.gbxFET.Size = new System.Drawing.Size(446, 256);
            this.gbxFET.TabIndex = 1;
            this.gbxFET.TabStop = false;
            this.gbxFET.Text = "Fisher Exact Test Results";
            // 
            // olvFETResults
            // 
            this.olvFETResults.CheckBoxes = false;
            this.olvFETResults.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olvFETResults.Location = new System.Drawing.Point(3, 16);
            this.olvFETResults.Name = "olvFETResults";
            this.olvFETResults.ShowGroups = false;
            this.olvFETResults.Size = new System.Drawing.Size(440, 237);
            this.olvFETResults.TabIndex = 2;
            this.olvFETResults.UseCompatibleStateImageBehavior = false;
            this.olvFETResults.View = System.Windows.Forms.View.Details;
            this.olvFETResults.VirtualMode = true;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbFile,
            this.tsbPreferences,
            this.tsbGroupCreation});
            this.toolStrip1.Location = new System.Drawing.Point(3, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(232, 25);
            this.toolStrip1.TabIndex = 0;
            // 
            // tsbFile
            // 
            this.tsbFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.btnSave,
            this.btnSaveAs,
            this.closeToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.tsbFile.Image = ((System.Drawing.Image)(resources.GetObject("tsbFile.Image")));
            this.tsbFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbFile.Name = "tsbFile";
            this.tsbFile.Size = new System.Drawing.Size(38, 22);
            this.tsbFile.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnOpenAnalysis,
            this.btnOpenDataFiles,
            this.numericDataFileToolStripMenuItem,
            this.sampleInfoFileToolStripMenuItem,
            this.btnOpenThresholdsFile});
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.openToolStripMenuItem.Text = "Open";
            // 
            // btnOpenAnalysis
            // 
            this.btnOpenAnalysis.Name = "btnOpenAnalysis";
            this.btnOpenAnalysis.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.btnOpenAnalysis.Size = new System.Drawing.Size(199, 22);
            this.btnOpenAnalysis.Text = "Analysis";
            // 
            // btnOpenDataFiles
            // 
            this.btnOpenDataFiles.Name = "btnOpenDataFiles";
            this.btnOpenDataFiles.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.O)));
            this.btnOpenDataFiles.Size = new System.Drawing.Size(199, 22);
            this.btnOpenDataFiles.Text = "Data Files";
            // 
            // numericDataFileToolStripMenuItem
            // 
            this.numericDataFileToolStripMenuItem.Name = "numericDataFileToolStripMenuItem";
            this.numericDataFileToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.numericDataFileToolStripMenuItem.Text = "Numeric Data File";
            this.numericDataFileToolStripMenuItem.Visible = false;
            // 
            // sampleInfoFileToolStripMenuItem
            // 
            this.sampleInfoFileToolStripMenuItem.Name = "sampleInfoFileToolStripMenuItem";
            this.sampleInfoFileToolStripMenuItem.Size = new System.Drawing.Size(199, 22);
            this.sampleInfoFileToolStripMenuItem.Text = "Sample Info File";
            this.sampleInfoFileToolStripMenuItem.Visible = false;
            // 
            // btnOpenThresholdsFile
            // 
            this.btnOpenThresholdsFile.Name = "btnOpenThresholdsFile";
            this.btnOpenThresholdsFile.Size = new System.Drawing.Size(199, 22);
            this.btnOpenThresholdsFile.Text = "Thresholds File";
            // 
            // btnSave
            // 
            this.btnSave.Name = "btnSave";
            this.btnSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.btnSave.Size = new System.Drawing.Size(195, 22);
            this.btnSave.Text = "Save";
            // 
            // btnSaveAs
            // 
            this.btnSaveAs.Name = "btnSaveAs";
            this.btnSaveAs.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.btnSaveAs.Size = new System.Drawing.Size(195, 22);
            this.btnSaveAs.Text = "Save As...";
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.closeToolStripMenuItem.Text = "Close";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // tsbPreferences
            // 
            this.tsbPreferences.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbPreferences.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnShowStatusLog,
            this.btnShowSigFETOnly,
            this.thresholdToolStripMenuItem,
            this.minGroupSizeForAutomatedGroupCreationToolStripMenuItem,
            this.pvalueThresholdToolStripMenuItem,
            this.toolStripSeparator1,
            this.aboutToolStripMenuItem});
            this.tsbPreferences.Image = ((System.Drawing.Image)(resources.GetObject("tsbPreferences.Image")));
            this.tsbPreferences.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbPreferences.Name = "tsbPreferences";
            this.tsbPreferences.Size = new System.Drawing.Size(81, 22);
            this.tsbPreferences.Text = "Preferences";
            this.tsbPreferences.Click += new System.EventHandler(this.OnPreferencesOpenShowHideLogForm);
            // 
            // btnShowStatusLog
            // 
            this.btnShowStatusLog.Name = "btnShowStatusLog";
            this.btnShowStatusLog.Size = new System.Drawing.Size(315, 22);
            this.btnShowStatusLog.Text = "Hide Status Log";
            // 
            // btnShowSigFETOnly
            // 
            this.btnShowSigFETOnly.Name = "btnShowSigFETOnly";
            this.btnShowSigFETOnly.Size = new System.Drawing.Size(315, 22);
            this.btnShowSigFETOnly.Text = "Show Only Significant FET Results";
            // 
            // thresholdToolStripMenuItem
            // 
            this.thresholdToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txtThreshold});
            this.thresholdToolStripMenuItem.Name = "thresholdToolStripMenuItem";
            this.thresholdToolStripMenuItem.Size = new System.Drawing.Size(315, 22);
            this.thresholdToolStripMenuItem.Text = "Threshold for numerical data";
            // 
            // txtThreshold
            // 
            this.txtThreshold.Enabled = false;
            this.txtThreshold.Name = "txtThreshold";
            this.txtThreshold.Size = new System.Drawing.Size(100, 23);
            this.txtThreshold.Text = "1.0";
            // 
            // minGroupSizeForAutomatedGroupCreationToolStripMenuItem
            // 
            this.minGroupSizeForAutomatedGroupCreationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txtMinSizeGroup});
            this.minGroupSizeForAutomatedGroupCreationToolStripMenuItem.Name = "minGroupSizeForAutomatedGroupCreationToolStripMenuItem";
            this.minGroupSizeForAutomatedGroupCreationToolStripMenuItem.Size = new System.Drawing.Size(315, 22);
            this.minGroupSizeForAutomatedGroupCreationToolStripMenuItem.Text = "Min. group size for automated group creation";
            // 
            // txtMinSizeGroup
            // 
            this.txtMinSizeGroup.Enabled = false;
            this.txtMinSizeGroup.Name = "txtMinSizeGroup";
            this.txtMinSizeGroup.Size = new System.Drawing.Size(100, 23);
            this.txtMinSizeGroup.Text = "2";
            // 
            // pvalueThresholdToolStripMenuItem
            // 
            this.pvalueThresholdToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txtPvalueThreshold});
            this.pvalueThresholdToolStripMenuItem.Name = "pvalueThresholdToolStripMenuItem";
            this.pvalueThresholdToolStripMenuItem.Size = new System.Drawing.Size(315, 22);
            this.pvalueThresholdToolStripMenuItem.Text = "p-value Threshold";
            // 
            // txtPvalueThreshold
            // 
            this.txtPvalueThreshold.Enabled = false;
            this.txtPvalueThreshold.Name = "txtPvalueThreshold";
            this.txtPvalueThreshold.Size = new System.Drawing.Size(100, 23);
            this.txtPvalueThreshold.Text = "0.05";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(312, 6);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(315, 22);
            this.aboutToolStripMenuItem.Text = "About Genome Fisher .NET";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.AboutToolStripMenuItemClick);
            // 
            // tsbGroupCreation
            // 
            this.tsbGroupCreation.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbGroupCreation.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAutoGroupCreation,
            this.btnCompoundGroupCreation});
            this.tsbGroupCreation.Image = ((System.Drawing.Image)(resources.GetObject("tsbGroupCreation.Image")));
            this.tsbGroupCreation.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbGroupCreation.Name = "tsbGroupCreation";
            this.tsbGroupCreation.Size = new System.Drawing.Size(101, 22);
            this.tsbGroupCreation.Text = "Group Creation";
            this.tsbGroupCreation.DropDownOpening += new System.EventHandler(this.TsbGroupCreationDropDownOpening);
            // 
            // btnAutoGroupCreation
            // 
            this.btnAutoGroupCreation.Name = "btnAutoGroupCreation";
            this.btnAutoGroupCreation.Size = new System.Drawing.Size(332, 22);
            this.btnAutoGroupCreation.Text = "Select Categories For Auto Group Creation";
            // 
            // btnCompoundGroupCreation
            // 
            this.btnCompoundGroupCreation.Name = "btnCompoundGroupCreation";
            this.btnCompoundGroupCreation.Size = new System.Drawing.Size(332, 22);
            this.btnCompoundGroupCreation.Text = "Select Categories For Compound Group Creation";
            // 
            // contextMenuGroupCollection
            // 
            this.contextMenuGroupCollection.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnChangeGroupCollectionName,
            this.btnAddNewGroupCollection,
            this.btnRemoveGroupCollection,
            this.toolStripSeparator2,
            this.btnCalcAllPairwiseComparisons,
            this.btnCalcAllNotInGroupComparisons});
            this.contextMenuGroupCollection.Name = "contextMenuGroupCollection";
            this.contextMenuGroupCollection.Size = new System.Drawing.Size(436, 120);
            this.contextMenuGroupCollection.Opening += new System.ComponentModel.CancelEventHandler(this.ContextMenuGroupCollectionOpening);
            // 
            // btnChangeGroupCollectionName
            // 
            this.btnChangeGroupCollectionName.Name = "btnChangeGroupCollectionName";
            this.btnChangeGroupCollectionName.Size = new System.Drawing.Size(435, 22);
            this.btnChangeGroupCollectionName.Text = "Change Group Collection Name";
            // 
            // btnAddNewGroupCollection
            // 
            this.btnAddNewGroupCollection.Name = "btnAddNewGroupCollection";
            this.btnAddNewGroupCollection.Size = new System.Drawing.Size(435, 22);
            this.btnAddNewGroupCollection.Text = "Add New Group Collection";
            // 
            // btnRemoveGroupCollection
            // 
            this.btnRemoveGroupCollection.Name = "btnRemoveGroupCollection";
            this.btnRemoveGroupCollection.Size = new System.Drawing.Size(435, 22);
            this.btnRemoveGroupCollection.Text = "Remove";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(432, 6);
            // 
            // btnCalcAllPairwiseComparisons
            // 
            this.btnCalcAllPairwiseComparisons.Name = "btnCalcAllPairwiseComparisons";
            this.btnCalcAllPairwiseComparisons.Size = new System.Drawing.Size(435, 22);
            this.btnCalcAllPairwiseComparisons.Text = "Perform All FET Comparisons For Group Collection";
            // 
            // btnCalcAllNotInGroupComparisons
            // 
            this.btnCalcAllNotInGroupComparisons.Name = "btnCalcAllNotInGroupComparisons";
            this.btnCalcAllNotInGroupComparisons.Size = new System.Drawing.Size(435, 22);
            this.btnCalcAllNotInGroupComparisons.Text = "Perform All VS NOT In Group FET Comparisons For Group Collection";
            // 
            // contextMenuGroups
            // 
            this.contextMenuGroups.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnChangeGroupName,
            this.btnRemovewGroup,
            this.toolStripSeparator3,
            this.btnCalcPairwiseComparisons,
            this.btnCalcNotInGroupComparisons,
            this.toolStripSeparator5,
            this.getAllSubgroupsForCategoryToolStripMenuItem,
            this.toolStripSeparator4,
            this.showGroupInfoToolStripMenuItem});
            this.contextMenuGroups.Name = "contextMenuGroups";
            this.contextMenuGroups.Size = new System.Drawing.Size(380, 154);
            this.contextMenuGroups.Opening += new System.ComponentModel.CancelEventHandler(this.ContextMenuGroupsOpening);
            // 
            // btnChangeGroupName
            // 
            this.btnChangeGroupName.Name = "btnChangeGroupName";
            this.btnChangeGroupName.Size = new System.Drawing.Size(379, 22);
            this.btnChangeGroupName.Text = "Change Group Name";
            // 
            // btnRemovewGroup
            // 
            this.btnRemovewGroup.Name = "btnRemovewGroup";
            this.btnRemovewGroup.Size = new System.Drawing.Size(379, 22);
            this.btnRemovewGroup.Text = "Remove Group";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(376, 6);
            // 
            // btnCalcPairwiseComparisons
            // 
            this.btnCalcPairwiseComparisons.Name = "btnCalcPairwiseComparisons";
            this.btnCalcPairwiseComparisons.Size = new System.Drawing.Size(379, 22);
            this.btnCalcPairwiseComparisons.Text = "Perform all pairwise FET comparisons for selected";
            // 
            // btnCalcNotInGroupComparisons
            // 
            this.btnCalcNotInGroupComparisons.Name = "btnCalcNotInGroupComparisons";
            this.btnCalcNotInGroupComparisons.Size = new System.Drawing.Size(379, 22);
            this.btnCalcNotInGroupComparisons.Text = "Perform vs NOT in group FET comparisons for all selected";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(376, 6);
            // 
            // getAllSubgroupsForCategoryToolStripMenuItem
            // 
            this.getAllSubgroupsForCategoryToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cbxCategories});
            this.getAllSubgroupsForCategoryToolStripMenuItem.Name = "getAllSubgroupsForCategoryToolStripMenuItem";
            this.getAllSubgroupsForCategoryToolStripMenuItem.Size = new System.Drawing.Size(379, 22);
            this.getAllSubgroupsForCategoryToolStripMenuItem.Text = "Auto create groups for category...";
            // 
            // cbxCategories
            // 
            this.cbxCategories.Enabled = false;
            this.cbxCategories.Name = "cbxCategories";
            this.cbxCategories.Size = new System.Drawing.Size(121, 23);
            this.cbxCategories.SelectedIndexChanged += new System.EventHandler(this.CbxCategoriesSelectedIndexChanged);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(376, 6);
            // 
            // showGroupInfoToolStripMenuItem
            // 
            this.showGroupInfoToolStripMenuItem.Name = "showGroupInfoToolStripMenuItem";
            this.showGroupInfoToolStripMenuItem.Size = new System.Drawing.Size(379, 22);
            this.showGroupInfoToolStripMenuItem.Text = "Show Group Info";
            // 
            // contextMenuFETGroups
            // 
            this.contextMenuFETGroups.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.removeSelectedFETComparisonsToolStripMenuItem,
            this.calculateFDRToolStripMenuItem,
            this.btnDetermineCompoundMarkers});
            this.contextMenuFETGroups.Name = "contextMenuFETGroups";
            this.contextMenuFETGroups.Size = new System.Drawing.Size(271, 70);
            // 
            // removeSelectedFETComparisonsToolStripMenuItem
            // 
            this.removeSelectedFETComparisonsToolStripMenuItem.Name = "removeSelectedFETComparisonsToolStripMenuItem";
            this.removeSelectedFETComparisonsToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.removeSelectedFETComparisonsToolStripMenuItem.Text = "Remove Selected FET Comparison(s)";
            // 
            // calculateFDRToolStripMenuItem
            // 
            this.calculateFDRToolStripMenuItem.Name = "calculateFDRToolStripMenuItem";
            this.calculateFDRToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
            this.calculateFDRToolStripMenuItem.Text = "Calculate FDR";
            // 
            // btnDetermineCompoundMarkers
            // 
            this.btnDetermineCompoundMarkers.Name = "btnDetermineCompoundMarkers";
            this.btnDetermineCompoundMarkers.Size = new System.Drawing.Size(270, 22);
            this.btnDetermineCompoundMarkers.Text = "Determine Compound MarkerIndices";
            // 
            // contextMenuFETResults
            // 
            this.contextMenuFETResults.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnSaveFETResults});
            this.contextMenuFETResults.Name = "contextMenuStrip1";
            this.contextMenuFETResults.Size = new System.Drawing.Size(122, 26);
            // 
            // btnSaveFETResults
            // 
            this.btnSaveFETResults.Name = "btnSaveFETResults";
            this.btnSaveFETResults.Size = new System.Drawing.Size(121, 22);
            this.btnSaveFETResults.Text = "Save as...";
            // 
            // bgw
            // 
            this.bgw.WorkerReportsProgress = true;
            // 
            // GenomeFisherMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1245, 562);
            this.Controls.Add(this.toolStripContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GenomeFisherMainForm";
            this.Text = "Genome Fisher .NET";
            this.toolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.BottomToolStripPanel.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.olvFETGroups)).EndInit();
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.gbxGroupSelection.ResumeLayout(false);
            this.contextMenuGroupSelection.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.splitContainer5.Panel1.ResumeLayout(false);
            this.splitContainer5.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).EndInit();
            this.splitContainer5.ResumeLayout(false);
            this.gbxGroupCollection.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.olvGroupCollections)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.olvGroups)).EndInit();
            this.gbxFET.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.olvFETResults)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.contextMenuGroupCollection.ResumeLayout(false);
            this.contextMenuGroups.ResumeLayout(false);
            this.contextMenuFETGroups.ResumeLayout(false);
            this.contextMenuFETResults.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslStatus;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton tsbFile;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sampleInfoFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem numericDataFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton tsbPreferences;
        private System.Windows.Forms.ToolStripMenuItem btnShowStatusLog;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private ListViewFF lvwGroupSelection;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ContextMenuStrip contextMenuGroupSelection;
        private System.Windows.Forms.ToolStripMenuItem btnCreateNewGroup;
        private System.Windows.Forms.ToolStripMenuItem btnClearSelection;
        private System.Windows.Forms.ContextMenuStrip contextMenuGroups;
        private System.Windows.Forms.ToolStripMenuItem btnChangeGroupName;
        private System.Windows.Forms.ToolStripMenuItem btnRemovewGroup;
        private System.Windows.Forms.ToolStripMenuItem thresholdToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox txtThreshold;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.ToolStripMenuItem getAllSubgroupsForCategoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripComboBox cbxCategories;
        private System.Windows.Forms.ToolStripMenuItem minGroupSizeForAutomatedGroupCreationToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox txtMinSizeGroup;
        private System.Windows.Forms.ContextMenuStrip contextMenuFETResults;
        private System.Windows.Forms.ToolStripMenuItem btnSaveFETResults;
        private System.ComponentModel.BackgroundWorker bgw;
        private System.Windows.Forms.ToolStripMenuItem btnOpenDataFiles;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.GroupBox gbxGroupSelection;
        private System.Windows.Forms.GroupBox gbxFET;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ContextMenuStrip contextMenuFETGroups;
        private System.Windows.Forms.ToolStripMenuItem removeSelectedFETComparisonsToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer5;
        private System.Windows.Forms.GroupBox gbxGroupCollection;
        private System.Windows.Forms.ToolStripMenuItem showGroupInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnCreateNewGroupIntoNewGroupCollection;
        private System.Windows.Forms.ContextMenuStrip contextMenuGroupCollection;
        private System.Windows.Forms.ToolStripMenuItem btnChangeGroupCollectionName;
        private System.Windows.Forms.ToolStripMenuItem btnAddNewGroupCollection;
        private System.Windows.Forms.ToolStripMenuItem btnRemoveGroupCollection;
        private System.Windows.Forms.ToolStripMenuItem btnCalcPairwiseComparisons;
        private System.Windows.Forms.ToolStripMenuItem btnCalcNotInGroupComparisons;
        private System.Windows.Forms.ToolStripDropDownButton tsbGroupCreation;
        private System.Windows.Forms.ToolStripMenuItem btnAutoGroupCreation;
        private System.Windows.Forms.ToolStripMenuItem pvalueThresholdToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox txtPvalueThreshold;
        private System.Windows.Forms.ToolStripMenuItem btnCompoundGroupCreation;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem btnCalcAllPairwiseComparisons;
        private System.Windows.Forms.ToolStripMenuItem btnCalcAllNotInGroupComparisons;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem btnOpenThresholdsFile;
        private System.Windows.Forms.ToolStripMenuItem calculateFDRToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnShowSigFETOnly;
        private System.Windows.Forms.ToolStripMenuItem btnDetermineCompoundMarkers;
        private System.Windows.Forms.ToolStripMenuItem btnSave;
        private System.Windows.Forms.ToolStripMenuItem btnSaveAs;
        private BrightIdeasSoftware.FastObjectListView olvFETGroups;
        private BrightIdeasSoftware.FastObjectListView olvFETResults;
        private BrightIdeasSoftware.FastObjectListView olvGroups;
        private BrightIdeasSoftware.FastObjectListView olvGroupCollections;
        private System.Windows.Forms.ToolStripMenuItem btnOpenAnalysis;
    }
}

