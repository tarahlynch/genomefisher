﻿using System;
using System.Collections;

namespace GenomeFisher.NET
{
    public class SingleMarker
    {
        private readonly int _a;
        private readonly int _b;
        private readonly int _c;
        private readonly ContingencyTable _contingencyTable;
        private readonly int _d;

        /// <summary>Marker index.</summary>
        private readonly int _marker;

        private readonly double _pValue;
        private readonly double _rateAB;
        private readonly double _rateCD;
        private readonly double _rateDifference;
        private readonly double _sensitivity;
        private readonly double _specificity;
        private readonly double _specificity2;

        /// <summary>Threshold for numerical data binarization.</summary>
        private readonly double _threshold;

        /// <summary>Binary absence/presence of marker in Group A.</summary>
        private BitArray _groupA;

        /// <summary>Binary absence/presence of marker in Group B.</summary>
        private BitArray _groupB;

        /// <summary>Marker binary absence/presence distribution for two groups - group A and group B.</summary>
        /// <param name="marker">Marker index.</param>
        /// <param name="threshold">Numerical data binarization threshold.</param>
        /// <param name="grpA">Numerical data for group A strains.</param>
        /// <param name="grpB">Numerical data for group B strains.</param>
        /// <param name="ct">Contigency table for marker.</param>
        public SingleMarker(
            int marker,
            double threshold,
            double[] grpA,
            double[] grpB,
            ContingencyTable ct)
        {
            _marker = marker;
            _threshold = threshold;
            _contingencyTable = ct;
            _a = _contingencyTable.GetA();
            _b = _contingencyTable.GetB();
            _c = _contingencyTable.GetC();
            _d = _contingencyTable.GetD();
            _rateAB = _a / (double) (_a + _b);
            _rateCD = _c / (double) (_c + _d);
            _rateDifference = Math.Abs(_rateAB - _rateCD);
            if (_a > _b)
            {
                _sensitivity = _a / (double) (_a + _b);
                _specificity = _d / (double) (_c + _d);
                _specificity2 = _a / (double) (_a + _c);
            }
            else
            {
                _sensitivity = _b / (double) (_a + _b);
                _specificity = _c / (double) (_c + _d);
                _specificity2 = _b / (double) (_b + _d);
            }
            _pValue = _contingencyTable.GetFisher2TailPermutationTest();
            CalculateStrainPresence(grpA, grpB);
        }

        public ContingencyTable ContingencyTable { get { return _contingencyTable; } }

        /// <summary>Get the marker index.</summary>
        public int MarkerIndex { get { return _marker; } }

        /// <summary>Get binary absence/presence of marker in Group A.</summary>
        public BitArray GroupAStrainDistr { get { return _groupA; } }

        /// <summary>Get binary absence/presence of marker in Group B.</summary>
        public BitArray GroupBStrainDistr { get { return _groupB; } }

        /// <summary>Get the threshold for numerical data binarization</summary>
        public double Threshold { get; set; }

        public bool MarkerType { get { return (_a >= _b); } }

        public int A { get { return _a; } }

        public int B { get { return _b; } }

        public int C { get { return _c; } }

        public int D { get { return _d; } }

        public double RateAB { get { return _rateAB; } }

        public double RateCD { get { return _rateCD; } }

        public double RateDifference { get { return _rateDifference; } }

        public double Sensitivity { get { return _sensitivity; } }

        public double Specificity { get { return _specificity; } }

        public double Specificity2 { get { return _specificity2; } }

        public double PValue { get { return _pValue; } }

        /// <summary>Get the binary absence/presence distribution across all strains in group A and group B.</summary>
        /// <param name="grpA">Numerical data for strains in group A.</param>
        /// <param name="grpB">Numerical data for strains in group B.</param>
        private void CalculateStrainPresence(double[] grpA, double[] grpB)
        {
            var gA = new bool[grpA.Length];
            for (int i = 0; i < grpA.Length; i++)
            {
                gA[i] = (grpA[i] >= _threshold);
            }
            _groupA = new BitArray(gA);

            var gB = new bool[grpB.Length];
            for (int i = 0; i < grpB.Length; i++)
            {
                gB[i] = (grpB[i] >= _threshold);
            }
            _groupB = new BitArray(gB);
        }

        public string[] GetBinaryDistr()
        {
            var tmp = new string[_groupA.Length + _groupB.Length];
            int count = 0;
            foreach (bool b in _groupA)
            {
                tmp[count++] = (b ? "1" : "0");
            }
            foreach (bool b in _groupB)
            {
                tmp[count++] = (b ? "1" : "0");
            }
            return tmp;
        }
    }
}