﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace GenomeFisher.NET
{
    /// <summary>Class responsible for reading numeric data file and sample info file, holding group information including FET results, and writing output.</summary>
    public class GenomeFisherAnalysis
    {
        /// <summary>List of categories derived from sample information.</summary>
        private readonly List<Category> _categoryList = new List<Category>();

        /// <summary>Filename of the sample info file.</summary>
        private readonly string _fCategory;

        /// <summary>Filename of the numeric data file.</summary>
        private readonly string _fNumeric;

        /// <summary>List of group collections with associated groups.</summary>
        private readonly List<GroupCollection> _groupCollections = new List<GroupCollection>();

        /// <summary>Sample ID from the first row of the numeric data file. </summary>
        private readonly List<string> _sampleID = new List<string>();

        /// <summary>List of genes from the first column in the numeric data file.</summary>
        private List<string> _genes = new List<string>();

        /// <summary>Headers from the sample info file.</summary>
        private List<string> _headersCategorical = new List<string>();

        /// <summary>Numeric data from the numeric data file.</summary>
        private List<double[]> _numericData = new List<double[]>();

        /// <summary>Temporary list of sample IDs to compare against sampleIDNumeric list of sample IDs for equality.</summary>
        private List<string> _sampleIDCategorical = new List<string>();

        /// <summary>Temporary list of sample IDs to compare against sampleIDCategorical list of sample IDs for equality.</summary>
        private List<string> _sampleIDNumeric = new List<string>();

        /// <summary>Sample info from the sample info file.</summary>
        private List<string[]> _sampleInfo = new List<string[]>();

        /// <summary>Individual marker thresholds.</summary>
        private double[] _thresholds;

        /// <summary>GenomeFisherAnalysis class constructor.</summary>
        /// <param name="filenameNumeric">Filename of the numeric data file.</param>
        /// <param name="filenameCategory">Filename of the sample info file.</param>
        /// <param name="thresholdNumeric">Threshold for determining what is positive or negative in calculation of counts for contingency tables.</param>
        /// <param name="thresholdPValue"> </param>
        public GenomeFisherAnalysis(string filenameNumeric,
                                    string filenameCategory,
                                    double thresholdNumeric = 1,
                                    double thresholdPValue = 0.05)
        {
            _fNumeric = filenameNumeric;
            _fCategory = filenameCategory;
            Threshold = thresholdNumeric;
            PValueThreshold = thresholdPValue;
        }

        public GenomeFisherAnalysis(string analysisFilename)
        {
            Read(analysisFilename);
        }

        #region File reading and validation

        /// <summary>Read both the sample info file containing category info and the numerical data file.</summary>
        public bool Read()
        {
            try
            {
                //EventStatData(this, new StatDataArgs("Reading sample information file."));
                ReadCategory();
                //EventStatData(this, new StatDataArgs("Reading numerical data file."));
                ReadNumeric();
                //EventStatData(this, new StatDataArgs("Checking whether file input is of valid format."));
                if (!(CheckEqualitySampleIDLists()))
                {
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                Clear();
                return false;
            }
            return true;
        }

        /// <summary>Read the numeric data file.</summary>
        private void ReadNumeric()
        {
            //initialize lists
            _genes = new List<string>();
            _sampleIDNumeric = new List<string>();
            _numericData = new List<double[]>();

            string[] lines = File.ReadAllLines(_fNumeric);
            string[] h = lines[0].Split('\t');

            for (int i = 1; i < h.Length; i++)
            {
                _sampleIDNumeric.Add(h[i]);
            }

            for (int i = 1; i < lines.Length; i++)
            {
                if (lines[i] == "")
                {
                    break;
                }
                string[] line = lines[i].Split('\t');
                _genes.Add(line[0]);
                var tmp = new List<double>();
                for (int j = 1; j < line.Length; j++)
                {
                    tmp.Add(double.Parse(line[j]));
                }
                _numericData.Add(tmp.ToArray());
            }
        }

        /// <summary>Read the sample info file.</summary>
        private void ReadCategory()
        {
            //initialize lists
            _sampleIDCategorical = new List<string>();
            _sampleInfo = new List<string[]>();
            _headersCategorical = new List<string>();

            string[] lines = File.ReadAllLines(_fCategory);
            string[] h = lines[0].Split('\t');

            for (int i = 1; i < h.Length; i++)
            {
                _headersCategorical.Add(h[i]);
            }

            for (int i = 1; i < lines.Length; i++)
            {
                if (lines[i] == "")
                {
                    break;
                }
                string[] line = lines[i].Split('\t');
                if (line[0] == "")
                {
                    break;
                }
                _sampleIDCategorical.Add(line[0]);
                var tmp = new List<string>();
                for (int j = 1; j < line.Length; j++)
                {
                    tmp.Add(line[j]);
                }
                _sampleInfo.Add(tmp.ToArray());
            }
            CreateCategories();
        }

        /// <summary>Check if the sampleIDNumeric and sampleIDCategorical lists are equal; return true if they are; false if not.</summary>
        /// <returns>Lists not equal return false; true otherwise.</returns>
        private bool CheckEqualitySampleIDLists()
        {
            if (!(_sampleIDCategorical.SequenceEqual(_sampleIDNumeric)))
            {
                if (_sampleIDCategorical.SequenceEqual(_genes))
                {
                    TransposeData();
                }
                else
                {
                    return false;
                }
            }

            _sampleID.AddRange(_sampleIDCategorical);
            _sampleIDCategorical.Clear();
            _sampleIDNumeric.Clear();
            //EventStatData(this, new StatDataArgs("All data headers and sample information validated."));
            return true;
        }

        /// <summary>Flip or transpose the data if it is found that the data is arranged with the sample IDs in the first column rather than the first row.</summary>
        private void TransposeData()
        {
            List<string> tmp = _sampleIDNumeric;
            _sampleIDNumeric = _genes;
            _genes = tmp;
            //create temp. list of double arrays to hold numeric information while it is rearranged
            var data = new List<double[]>();
            //go down a column adding numerical values to a temp. list of doubles when finished add the array of the list to the growing double array list
            //move onto the next column and do the same
            //NOTE: Assuming that all columns and rows are of the same length in the data file.
            //iterate through the rows
            for (int i = 0; i < _numericData[0].Length; i++)
            {
                //iterate through columns
                var dl = new List<double>();
                for (int j = 0; j < _numericData.Count; j++)
                {
                    dl.Add(_numericData[j][i]);
                }
                data.Add(dl.ToArray());
            }
            //assign rearranged numeric data to numericData list
            _numericData = data;
        }

        /// <summary>Create new Categories from sample information file information.</summary>
        private void CreateCategories()
        {
            for (int i = 0; i < _headersCategorical.Count; i++)
            {
                var cat = new Category(_headersCategorical[i], i, ref _sampleInfo);
                _categoryList.Add(cat);
            }
        }

        #endregion

        /// <summary>Raw numerical data from numerical data file.</summary>
        public List<double[]> RawData { get { return _numericData; } }

        public List<string> Samples { get { return _sampleID; } }

        /// <summary>Get the gene names within the dataset.</summary>
        public string[] Genes { get { return _genes.ToArray(); } }

        /// <summary>List of categories.</summary>
        public List<Category> Categories { get { return _categoryList; } }

        /// <summary>Get list of group collections with associated groups.</summary>
        public List<GroupCollection> GroupCollections { get { return _groupCollections; } }

        /// <summary>Threshold for determining what is positive and negative for calculating counts for contingency tables.</summary>
        public double Threshold { get; set; }

        /// <summary>Threshold for p-value.</summary>
        public double PValueThreshold { get; set; }

        /// <summary>Get or set the minimum group size when automatically creating groups from the items in a category.</summary>
        public int MinGroupSize { get; set; }

        /// <summary>Get the individual marker thresholds.</summary>
        public double[] Thresholds { get { return _thresholds; } }

        /// <summary>Read the file containing the thresholds and get the thresholds for each gene.</summary>
        /// <param name="thresholdFilepath"></param>
        public void ReadThresholds(string thresholdFilepath)
        {
            try
            {
                string[] lines = File.ReadAllLines(thresholdFilepath);
                var gList = new List<string>();
                var thresholdList = new List<double>();
                foreach (string line in lines)
                {
                    string[] l = line.Split('\t');
                    if (l[0] == "")
                    {
                        break;
                    }
                    gList.Add(l[0]);
                    thresholdList.Add(double.Parse(l[1]));
                }
                //check if number of genes in thresholds file and data file correspond
                if (gList.Count != _genes.Count)
                {
                    throw new Exception(
                        "Number of genes in thresholds file does not match number of genes in data file.");
                }

                //make sure that every gene in data file is also in the thresholds file
                foreach (string gene in _genes)
                {
                    if (!gList.Contains(gene))
                    {
                        throw new Exception("Thresholds file does not contain gene '" + gene +
                                            "'. Please verify fidelity of thresholds file.");
                    }
                }

                _thresholds = thresholdList.ToArray();
                //rearrange any thresholds depending on the order of genes in the thresholds file
                for (int i = 0; i < gList.Count; i++)
                {
                    string gene = gList[i];
                    int index = _genes.IndexOf(gene);
                    if (index != i)
                    {
                        _thresholds[index] = thresholdList[i];
                    }
                }
            }
            catch (Exception ex)
            {
                _thresholds = null;
            }
        }

        /// <summary>Create a new empty group collection with a name specified but no groups specified.</summary>
        /// <param name="gcName">Group collection name.</param>
        public GroupCollection CreateNewGroupCollection(string gcName)
        {
            var gc = new GroupCollection(this, gcName);
            _groupCollections.Add(gc);
            return gc;
        }

        /// <summary>Create a new group collection with a default name and a group specified.</summary>
        /// <param name="g">Group to be part of new group collection.</param>
        public void CreateNewGroupCollection(Group g)
        {
            var gc = new GroupCollection(this, g);
            _groupCollections.Add(gc);
        }

        public void RemoveGroupCollection(List<GroupCollection> groupCollections)
        {
            foreach (GroupCollection groupCollection in groupCollections)
            {
                _groupCollections.Remove(groupCollection);
            }
        }

        /// <summary>Clear all data structures in this class.</summary>
        private void Clear()
        {
            if (_headersCategorical != null)
            {
                _headersCategorical.Clear();
            }
            if (_groupCollections != null)
            {
                _groupCollections.Clear();
            }
        }

        /// <summary>Calculate all FET stats for all pairwise comparisons between all groups in a group collection of specified index.</summary>
        /// <param name="groupCollection">Index of group collection.</param>
        public void CalcFisherStatsPairwiseAll(GroupCollection groupCollection)
        {
            //get all pairwise non-redundant comparisons of the created groups
            for (int i = 0; i < groupCollection.Groups.Count - 1; i++)
            {
                for (int j = i + 1; j < groupCollection.Groups.Count; j++)
                {
                    FETGroup tmp;
                    if (_thresholds != null && _thresholds.Length == _genes.Count)
                    {
                        tmp = new FETGroup(this,
                                           groupCollection,
                                           groupCollection.Groups[i],
                                           groupCollection.Groups[j],
                                           PValueThreshold,
                                           true);
                    }
                    else
                    {
                        tmp = new FETGroup(this,
                                           groupCollection,
                                           groupCollection.Groups[i],
                                           groupCollection.Groups[j],
                                           PValueThreshold);
                    }

                    tmp.CalcFisherValues();
                    groupCollection.Add(tmp);
                }
            }
        }

        /// <summary>Calculate all FET stats for all pairwise comparisons between all selected groups within a group collection of specified index.</summary>
        /// <param name="groupCollection"> </param>
        /// <param name="groups"> </param>
        public void CalcFisherStatsPairwiseAll(GroupCollection groupCollection, List<Group> groups)
        {
            //get all pairwise non-redundant comparisons of the created groups
            for (int i = 0; i < groups.Count - 1; i++)
            {
                for (int j = i + 1; j < groups.Count; j++)
                {
                    FETGroup tmp;
                    if (_thresholds != null && _thresholds.Length == _genes.Count)
                    {
                        tmp = new FETGroup(this, groupCollection, groups[i], groups[j], PValueThreshold, true);
                    }
                    else
                    {
                        tmp = new FETGroup(this, groupCollection, groups[i], groups[j], Threshold);
                    }
                    tmp.CalcFisherValues();
                    groupCollection.Add(tmp);
                }
            }
        }

        /// <summary>Calculate all FET stats for all groups in a specified group collection vs samples not in the groups.</summary>
        /// <param name="groupCollection">Group collection index.</param>
        public void CalculateVsNotInGroup(GroupCollection groupCollection)
        {
            //get all pairwise non-redundant comparisons of the created groups
            foreach (Group g in groupCollection.Groups)
            {
                Group gNot = GetNotGroup(g);
                FETGroup tmp;
                if (_thresholds != null && _thresholds.Length == _genes.Count)
                {
                    tmp = new FETGroup(this, groupCollection, g, gNot, PValueThreshold, true);
                }
                else
                {
                    tmp = new FETGroup(this, groupCollection, g, gNot, PValueThreshold);
                }
                tmp.CalcFisherValues();
                groupCollection.Add(tmp);
            }
        }

        /// <summary>Calculate all FET stats for the selected groups from a specified group collection vs samples not in the specified groups.</summary>
        public void CalculateVsNotInGroup(GroupCollection groupCollection, List<Group> groups)
        {
            //get all pairwise non-redundant comparisons of the created groups
            foreach (Group inGroup in groups)
            {
                Group notGroup = GetNotGroup(inGroup);
                FETGroup tmp;
                if (_thresholds != null && _thresholds.Length == _genes.Count)
                {
                    tmp = new FETGroup(this, groupCollection, inGroup, notGroup, PValueThreshold, true);
                }
                else
                {
                    tmp = new FETGroup(this, groupCollection, inGroup, notGroup, PValueThreshold);
                }
                tmp.CalcFisherValues();
                groupCollection.Add(tmp);
            }
        }

        /// <summary>Get the group that has all the samples not present within the specified group.</summary>
        /// <param name="inGroup">Group that you want to get all of the samples not in the group for.</param>
        /// <returns>Group with all samples not in specified group.</returns>
        private Group GetNotGroup(Group inGroup)
        {
            return new Group(GetNotInGroupStrains(inGroup.Strains), "Not in " + inGroup.Name);
        }

        /// <summary></summary>
        /// <param name="iGroup"></param>
        /// <returns></returns>
        private int[] GetNotInGroupStrains(IEnumerable<int> iGroup)
        {
            var liGroup = new List<int>();
            liGroup.AddRange(iGroup);
            var tmp = new List<int>();
            for (int i = 0; i < _sampleID.Count; i++)
            {
                if (!liGroup.Contains(i))
                {
                    tmp.Add(i);
                }
            }
            return tmp.ToArray();
        }

        #region Work File Reading and Writing

        /// <summary>Write all information from the numerical data file, sample information file and the tests, groups and group collections made by the user to a work file.</summary>
        /// <param name="savePath">Save path for the work file.</param>
        public void Write(string savePath)
        {
            //List<string> lines = new List<string>();
            var writer = new StreamWriter(savePath);
            //Line 1: numeric threshold; p-value threshold; min. group size
            if (_thresholds != null && _thresholds.Length == _genes.Count)
            {
                var tmp = new List<string>();
                foreach (double d in _thresholds)
                {
                    tmp.Add(d.ToString(CultureInfo.InvariantCulture));
                }
                writer.WriteLine("{0}\t{1}\t{2}\t{3}", Threshold, PValueThreshold, MinGroupSize, string.Join(",", tmp));
            }
            else
            {
                writer.WriteLine("{0}\t{1}\t{2}", Threshold, PValueThreshold, MinGroupSize);
            }
            //Line 2: genes
            writer.WriteLine(string.Join("\t", _genes));
            //Line 3: samples
            writer.WriteLine(string.Join("\t", _sampleID));
            //Line 4: categories
            writer.WriteLine(string.Join("\t", _headersCategorical));
            //Line 5: numerical data; comma delimited within sets; semicolon delimited between sets
            var numData = new List<string>();
            foreach (var dArr in _numericData)
            {
                var set = new List<string>();
                foreach (double d in dArr)
                {
                    set.Add(d.ToString(CultureInfo.InvariantCulture));
                }
                numData.Add(string.Join(",", set));
            }
            writer.WriteLine(string.Join(";", numData));
            //Line 6: sample info; comma delimited within sets; semicolon delimited between sets
            var sInfo = new List<string>();
            foreach (var sArr in _sampleInfo)
            {
                sInfo.Add(string.Join(",", sArr));
            }
            writer.WriteLine(string.Join(";", sInfo));
            writer.WriteLine(_groupCollections.Count);
            //Group collections and associated groups
            foreach (GroupCollection gc in _groupCollections)
            {
                //index of group collection; name of group collection; number of groups in group collection
                writer.WriteLine("{0}\t{1}\t{2}", _groupCollections.IndexOf(gc), gc.Name, gc.Groups.Count);
                foreach (Group g in gc.Groups)
                {
                    var tmp = new List<string>();
                    foreach (int i in g.Strains)
                    {
                        tmp.Add(i.ToString(CultureInfo.InvariantCulture));
                    }
                    writer.WriteLine("{0}\t{1}", g.Name, string.Join(",", tmp));
                }
                writer.WriteLine(gc.FETGroups.Count);
                //last thing to be written will be all of the FETGroup information
                foreach (FETGroup f in gc.FETGroups)
                {
                    writer.WriteLine(string.Format("{0}\t{1}\t{2}\t{3}\t{4}",
                                                   gc.Groups.IndexOf(f.GroupA),
                                                   gc.Groups.IndexOf(f.GroupB),
                                                   f.MostExtremePValue,
                                                   //most extreme p-value
                                                   f.Threshold,
                                                   //numerical threshold
                                                   f.PValueThreshold));

                    //group A info - make sure that information is present in the group collection; if index is -1 use this info, otherwise use group info from group collection
                    var tmp = new List<string>();
                    foreach (int i in f.GroupA.Strains)
                    {
                        tmp.Add(i.ToString(CultureInfo.InvariantCulture));
                    }
                    writer.WriteLine(f.GroupA.Name + "\t" + string.Join(",", tmp));
                    tmp.Clear();
                    foreach (int i in f.GroupB.Strains)
                    {
                        tmp.Add(i.ToString(CultureInfo.InvariantCulture));
                    }
                    writer.WriteLine(f.GroupB.Name + "\t" + string.Join(",", tmp));
                    var sPValues = new List<string>();
                    var sA = new List<string>();
                    var sB = new List<string>();
                    var sC = new List<string>();
                    var sD = new List<string>();
                    var sGeneIndex = new List<string>();
                    foreach (ContingencyTable ct in f.ContingencyTables)
                    {
                        //get all of the contingency table variables
                        sPValues.Add(ct.GetFisher2TailPermutationTest().ToString(CultureInfo.InvariantCulture));
                        sA.Add(ct.GetA().ToString(CultureInfo.InvariantCulture));
                        sB.Add(ct.GetB().ToString(CultureInfo.InvariantCulture));
                        sC.Add(ct.GetC().ToString(CultureInfo.InvariantCulture));
                        sD.Add(ct.GetD().ToString(CultureInfo.InvariantCulture));
                        sGeneIndex.Add(ct.GeneIndex.ToString(CultureInfo.InvariantCulture));
                    }
                    //write all p-values for the FETGroup to a line
                    writer.WriteLine(string.Join(",", sPValues));
                    //write all A values from the ContingencyTables to a line
                    writer.WriteLine(string.Join(",", sA));
                    //write all B values from the ContingencyTables to a line
                    writer.WriteLine(string.Join(",", sB));
                    //write all C values from the ContingencyTables to a line
                    writer.WriteLine(string.Join(",", sC));
                    //write all D values from the ContingencyTables to a line
                    writer.WriteLine(string.Join(",", sD));
                    //write all gene indices for each ContingencyTable
                    writer.WriteLine(string.Join(",", sGeneIndex));
                }
            }

            writer.Close();
        }

        /// <summary>Read a work file and reconstruct all groups, FET comparisons, raw data, sample information and group collection information.</summary>
        /// <param name="workFile">Work file path.</param>
        private void Read(string workFile)
        {
            var reader = new StreamReader(workFile);
            //string[] lines = File.ReadAllLines(workFile);
            //Line 1: numeric threshold; p-value threshold; min. group size
            string readLine = reader.ReadLine();
            if (readLine != null)
            {
                string[] line0 = readLine.Split('\t');
                Threshold = double.Parse(line0[0]);
                PValueThreshold = double.Parse(line0[1]);
                MinGroupSize = int.Parse(line0[2]);
                //get the gene-wise thresholds if they are present
                if (line0.Length == 4)
                {
                    string[] t = line0[3].Split(',');
                    var tList = new List<double>();
                    foreach (string s in t)
                    {
                        tList.Add(double.Parse(s));
                    }
                    _thresholds = tList.ToArray();
                }
            }
            //Line 2: genes
            string line = reader.ReadLine();
            if (line != null) _genes.AddRange(line.Split('\t'));
            //Line 3: samples
            line = reader.ReadLine();
            if (line != null) _sampleID.AddRange(line.Split('\t'));
            //Line 4: categories
            _headersCategorical = new List<string>();
            string readLine1 = reader.ReadLine();
            if (readLine1 != null) _headersCategorical.AddRange(readLine1.Split('\t'));
            string line1 = reader.ReadLine();
            if (line1 != null)
            {
                string[] numSets = line1.Split(';');
                foreach (string numSet in numSets)
                {
                    string[] setData = numSet.Split(',');
                    var dSetData = new List<double>();
                    foreach (string s in setData)
                    {
                        dSetData.Add(double.Parse(s));
                    }
                    _numericData.Add(dSetData.ToArray());
                }
            }
            line = reader.ReadLine();
            if (line != null)
            {
                string[] sampleSets = line.Split(';');
                foreach (string sampleSet in sampleSets)
                {
                    _sampleInfo.Add(sampleSet.Split(','));
                }
            }
            CreateCategories();
            line = reader.ReadLine();
            int numGCs = -1;
            if (line != null)
            {
                numGCs = int.Parse(line);
            }
            //int lineCount = 7;
            while (_groupCollections.Count < numGCs)
            {
                line = reader.ReadLine();
                if (line == null)
                    continue;

                string[] lineGC = line.Split('\t');
                int gci = int.Parse(lineGC[0]);
                string gcName = lineGC[1];
                GroupCollection gc = CreateNewGroupCollection(gcName);
                int gCount = int.Parse(lineGC[2]);
                while (gc.Groups.Count < gCount)
                {
                    line = reader.ReadLine();
                    if (line == null) continue;
                    string[] gLine = line.Split('\t');
                    string[] sStrains = gLine[1].Split(',');
                    var strains = new int[sStrains.Length];
                    for (int i = 0; i < sStrains.Length; i++)
                    {
                        strains[i] = (int.Parse(sStrains[i]));
                    }
                    gc.Add(new Group(strains.ToArray(), gLine[0]));
                }

                line = reader.ReadLine();
                int numFETGroups = -1;
                if (line != null)
                    numFETGroups = int.Parse(line);
                while (gc.FETGroups.Count < numFETGroups)
                {
                    string[] fetInfo = reader.ReadLine().Split('\t');
                    int gA = int.Parse(fetInfo[0]);
                    int gB = int.Parse(fetInfo[1]);
                    double mostExtremePVal = double.Parse(fetInfo[2]);
                    double numThreshold = double.Parse(fetInfo[3]);
                    double thresholdPVal = double.Parse(fetInfo[4]);
                    //get Group A
                    Group grpA;
                    if (gA == -1)
                    {
                        string[] sGrpA = reader.ReadLine().Split('\t');
                        string[] sStrains = sGrpA[1].Split(',');
                        var strains = new List<int>();
                        foreach (string s in sStrains)
                        {
                            strains.Add(int.Parse(s));
                        }
                        grpA = new Group(strains.ToArray(), sGrpA[0]);
                    }
                    else
                    {
                        reader.ReadLine();
                        grpA = gc.Groups[gA];
                    }
                    //get Group B
                    Group grpB;
                    if (gB == -1)
                    {
                        string[] sGrpB = reader.ReadLine().Split('\t');
                        string[] sStrains = sGrpB[1].Split(',');
                        var strains = new List<int>();
                        foreach (string s in sStrains)
                        {
                            strains.Add(int.Parse(s));
                        }
                        grpB = new Group(strains.ToArray(), sGrpB[0]);
                    }
                    else
                    {
                        reader.ReadLine();
                        grpB = gc.Groups[gB];
                    }
                    //get p-values
                    string[] sPVal = reader.ReadLine().Split(',');
                    var pValues = new List<double>();
                    foreach (string s in sPVal)
                    {
                        pValues.Add(double.Parse(s));
                    }
                    //get A values for contingency tables
                    string[] sA = reader.ReadLine().Split(',');
                    var a = new List<int>();
                    foreach (string s in sA)
                    {
                        a.Add(int.Parse(s));
                    }
                    //get B values for contingency tables
                    string[] sB = reader.ReadLine().Split(',');
                    var b = new List<int>();
                    foreach (string s in sB)
                    {
                        b.Add(int.Parse(s));
                    }
                    //get C values for contingency tables
                    string[] sC = reader.ReadLine().Split(',');
                    var c = new List<int>();
                    foreach (string s in sC)
                    {
                        c.Add(int.Parse(s));
                    }
                    //get D values for contingency tables
                    string[] sD = reader.ReadLine().Split(',');
                    var d = new List<int>();
                    foreach (string s in sD)
                    {
                        d.Add(int.Parse(s));
                    }
                    string[] sGeneIndex = reader.ReadLine().Split(',');
                    var geneIndex = new List<int>();
                    foreach (string s in sGeneIndex)
                    {
                        geneIndex.Add(int.Parse(s));
                    }
                    var fet = new FETGroup(this, gc, grpA, grpB, numThreshold, thresholdPVal, mostExtremePVal, a, b, c, d, geneIndex, pValues);
                    gc.FETGroups.Add(fet);
                }
            }

            reader.Close();
        }

        #endregion

//         public void SaveToBinFile(string filename)
//         {
//             IFormatter formatter = new BinaryFormatter();
//             Stream stream = new FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.None);
//             formatter.Serialize(stream, this);
//             stream.Close();
//         }
    }
}