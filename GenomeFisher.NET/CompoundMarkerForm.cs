﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using BrightIdeasSoftware;

namespace GenomeFisher.NET
{
    public partial class CompoundMarkerForm : Form
    {
        private readonly GenomeFisherAnalysis _analysis;
        private readonly BackgroundWorker _bgwOptimize = new BackgroundWorker();
        private readonly BackgroundWorker _bgwOptimizeExhaustive = new BackgroundWorker();

        /// <summary>Holds all of the compound markers.</summary>
        private readonly CompoundMarkerCollection _compoundMarkers;

        private readonly FETGroup _fetGroup;
        private readonly List<int> _markerIndices = new List<int>();
        private readonly TypedObjectListView<CompoundMarker> _olvCompoundMarker;

        private readonly List<SingleMarker> _singleMarkers = new List<SingleMarker>();
        private readonly CompoundMarkerThresholds _thresholds;

        private int _subsetSize = 2;

        /// <summary>Constructor for frmCompoundMarker.</summary>
        /// <param name="analysis">GenomeFisherAnalysis object with all information relating to dataset and stats calculated so far.</param>
        /// <param name="fetGroup">Selected FETGroup/Comparison.</param>
        /// <param name="threshold">Threshold for p-value significance. Also used for determining what markers should be considered when creating compound markers.</param>
        /// <param name="thresholds"> </param>
        public CompoundMarkerForm(GenomeFisherAnalysis analysis, FETGroup fetGroup, double threshold, CompoundMarkerThresholds thresholds)
        {
            InitializeComponent();

            _analysis = analysis;
            _fetGroup = fetGroup;
            _thresholds = thresholds;

            _olvCompoundMarker = new TypedObjectListView<CompoundMarker>(_olv);
            _olv.FullRowSelect = true;
            _olv.GridLines = true;
            _olv.IncludeColumnHeadersInCopy = true;
            GetSingleMarkers(threshold);

            GetThresholds();

            _compoundMarkers = new CompoundMarkerCollection(_markerIndices, _singleMarkers, _subsetSize);
            SetupListView();

            SetTextBoxInputValidationEvents();
            SetBackgroundWorkerEvents();
            btnFindBestCompoundMarker.Click += (sender, args) =>
                                                   {
                                                       SetThresholds();
                                                       _compoundMarkers.GetHighestPositiveRateDifferential(0.001);
                                                       PopulateListview();
                                                       UpdateStatus();
                                                   };

            resultsToolStripMenuItem.Click += (sender, args) => SaveResults();
            resultsBinaryDataToolStripMenuItem.Click += (sender, args) => SaveResultsAndBinary();
            binaryDataToolStripMenuItem.Click += (sender, args) => SaveBinary();

            SetOptmizeCompoundMarkerEvents();

            Load += (sender, args) =>
                        {
                            Text = string.Format("Compound Marker Determination - {0} VS {1}",
                                                 _fetGroup.GroupAName,
                                                 _fetGroup.GroupBName);
                            PopulateListview();
                            UpdateStatus();
                        };

            btnDeconstruct.Click += (sender, args) =>
                                        {
                                            if (_olvCompoundMarker.SelectedObjects.Count == 1)
                                            {
                                                new CompoundMarkerForm(_analysis, _fetGroup, _olvCompoundMarker.SelectedObject, _thresholds).Show();
                                            }
                                        };

            btnCopyPatternToClipboard.Click += (sender, args) =>
                                                   {
                                                       if (_olvCompoundMarker.SelectedObjects.Count == 1)
                                                       {
                                                           CopyPatternToClipboard(_olvCompoundMarker.SelectedObject);
                                                       }
                                                   };
        }

        private void CopyPatternToClipboard(CompoundMarker compoundMarker)
        {

            var sb = new System.Text.StringBuilder();
            sb.Append('\t');
            sb.AppendLine(string.Join("", compoundMarker.BinaryDistribution));
            foreach (var sm in compoundMarker.SingleMarkers)
            {
                sb.Append(sm.MarkerIndex);
                sb.Append('\t');
                sb.AppendLine(string.Join("", sm.GetBinaryDistr()));
            }
            Clipboard.SetText(sb.ToString());
        }

        private CompoundMarkerForm(GenomeFisherAnalysis analysis,FETGroup fetGroup , CompoundMarker compoundMarker, CompoundMarkerThresholds thresholds)
        {
            InitializeComponent();

            _analysis = analysis;
            _fetGroup = fetGroup;
            _thresholds = thresholds;

            _olvCompoundMarker = new TypedObjectListView<CompoundMarker>(_olv);
            _olv.FullRowSelect = true;
            _olv.GridLines = true;
            _olv.IncludeColumnHeadersInCopy = true;
            //GetSingleMarkers(threshold);

            int count = 0;
            foreach (SingleMarker marker in compoundMarker.SingleMarkers)
            {
                _singleMarkers.Add(marker);
                _markerIndices.Add(count++);
            }

            GetThresholds();

            _subsetSize = compoundMarker.SingleMarkers.Count - 1;
            _compoundMarkers = new CompoundMarkerCollection(_markerIndices, _singleMarkers, _subsetSize);
            _compoundMarkers.Markers.Add(compoundMarker);
            SetupListView();

            SetTextBoxInputValidationEvents();
            SetBackgroundWorkerEvents();
            btnFindBestCompoundMarker.Click += (sender, args) =>
                                                   {
                                                       SetThresholds();
                                                       _compoundMarkers.GetHighestPositiveRateDifferential(0.001);
                                                       PopulateListview();
                                                       UpdateStatus();
                                                   };

            resultsToolStripMenuItem.Click += (sender, args) => SaveResults();
            resultsBinaryDataToolStripMenuItem.Click += (sender, args) => SaveResultsAndBinary();
            binaryDataToolStripMenuItem.Click += (sender, args) => SaveBinary();

            SetOptmizeCompoundMarkerEvents();

            Load += (sender, args) =>
                        {
                            Text = string.Format("Compound Marker Deconstruction of {0}",
                                                 GetMarkerNames(compoundMarker));
                            BtnRunClick(null, null);
                        };
            btnDeconstruct.Click += (sender, args) =>
            {
                if (_olvCompoundMarker.SelectedObjects.Count == 1)
                {
                    new CompoundMarkerForm(_analysis, _fetGroup, _olvCompoundMarker.SelectedObject, _thresholds).Show();
                }
            };
        }

        private void SetOptmizeCompoundMarkerEvents()
        {
            _bgwOptimize.DoWork += (sender, args) =>
                                       {
                                           var list = (List<CompoundMarker>) args.Argument;
                                           foreach (CompoundMarker cm in list)
                                           {
                                               _compoundMarkers.OptimizeCompoundMarker(cm, 0.001);
                                           }
                                       };

            _bgwOptimize.RunWorkerCompleted += (sender, args) => OnBgwRunCompleted();

            btnOptimizeCompoundMarker.Click += (sender, args) => OnBtnOptimizeCompoundMarkerOnClick(_bgwOptimize);

            _bgwOptimizeExhaustive.DoWork += (sender, args) =>
                                                 {
                                                     var list = (List<CompoundMarker>) args.Argument;
                                                     var setsTried = new HashSet<string>();
                                                     foreach (CompoundMarker cm in list)
                                                     {
                                                         Debug.WriteLine(">>Start compound marker creation {0} with {1:p}", GetMarkerNames(cm), cm.RateDifference);
                                                         _compoundMarkers.OptimizeCompoundMarkerExhaustive(cm, cm.RateDifference, ref setsTried);
                                                     }
                                                 };

            _bgwOptimizeExhaustive.RunWorkerCompleted += (sender, args) => OnBgwRunCompleted();

            btnOptimizeExhaustive.Click += (sender, args) => OnBtnOptimizeCompoundMarkerOnClick(_bgwOptimizeExhaustive);

            btnCreateCompoundMarker.Click += (sender, args) =>
                                                 {
                                                     if (_olvCompoundMarker.SelectedObjects.Count <= 1) 
                                                         return;
                                                     var list = new List<CompoundMarker>();
                                                     foreach (CompoundMarker marker in _olvCompoundMarker.SelectedObjects)
                                                     {
                                                         list.Add(marker);
                                                     }
                                                     _compoundMarkers.CreateCompoundMarker(list);
                                                     OnBgwRunCompleted();
                                                 };
        }

        private void OnBtnOptimizeCompoundMarkerOnClick(BackgroundWorker bgw)
        {
            if (_olvCompoundMarker.SelectedObjects.Count == 0)
                return;
            var list = new List<CompoundMarker>();
            foreach (CompoundMarker cm in _olvCompoundMarker.SelectedObjects)
            {
                list.Add(cm);
            }
            EnableControls(false);

            ProgressBar1.Visible = true;
            ProgressBar1.Enabled = true;

            ProgressBar1.Style = ProgressBarStyle.Marquee;
            ProgressBar1.MarqueeAnimationSpeed = 100;

            bgw.RunWorkerAsync(list);
        }

        private void SetBackgroundWorkerEvents()
        {
            _bgw.DoWork += (sender, args) => _compoundMarkers.CreateCompoundMarkers(_bgw, _subsetSize, _thresholds);
            _bgw.ProgressChanged += (sender, args) => ProgressBar1.PerformStep();
            _bgw.RunWorkerCompleted += (sender, args) => OnBgwRunCompleted();
        }

        private void OnBgwRunCompleted()
        {
            ProgressBar1.MarqueeAnimationSpeed = 0;
            ProgressBar1.Visible = false;
            ProgressBar1.Enabled = false;
            ProgressBar1.Style = ProgressBarStyle.Continuous;
            EnableControls(true);
            PopulateListview();
            UpdateStatus();
        }

        private void SetTextBoxInputValidationEvents()
        {
            //textbox input validation
            txtSensitivityMarker.KeyPress += (sender, args) => KeyPressTxtThresholds(txtSensitivityMarker, args);
            txtSpecificityMarker.KeyPress += (sender, args) => KeyPressTxtThresholds(txtSpecificityMarker, args);
            txtDifferenceMarker.KeyPress += (sender, args) => KeyPressTxtThresholds(txtDifferenceMarker, args);
            txtSensitivityResults.KeyPress += (sender, args) => KeyPressTxtThresholds(txtSensitivityResults, args);
            txtSpecificityResults.KeyPress += (sender, args) => KeyPressTxtThresholds(txtSpecificityResults, args);
            txtDifferenceResults.KeyPress += (sender, args) => KeyPressTxtThresholds(txtDifferenceResults, args);
            //textbox lostfocus - finalize data input
            txtSensitivityMarker.LostFocus +=
                (sender, args) => KeyPressTxtThresholds(txtSensitivityMarker, new KeyPressEventArgs((char) Keys.Enter));
            txtSpecificityMarker.LostFocus +=
                (sender, args) => KeyPressTxtThresholds(txtSpecificityMarker, new KeyPressEventArgs((char) Keys.Enter));
            txtDifferenceMarker.LostFocus +=
                (sender, args) => KeyPressTxtThresholds(txtDifferenceMarker, new KeyPressEventArgs((char) Keys.Enter));
            txtSensitivityResults.LostFocus +=
                (sender, args) => KeyPressTxtThresholds(txtSensitivityResults, new KeyPressEventArgs((char) Keys.Enter));
            txtSpecificityResults.LostFocus +=
                (sender, args) => KeyPressTxtThresholds(txtSpecificityResults, new KeyPressEventArgs((char) Keys.Enter));
            txtDifferenceResults.LostFocus +=
                (sender, args) => KeyPressTxtThresholds(txtDifferenceResults, new KeyPressEventArgs((char) Keys.Enter));

            txtMaxSize.KeyPress += (sender, args) => ValidateMaxSizeTextboxInput(args);
            txtMaxSize.LostFocus += (sender, args) => ValidateMaxSizeTextboxInput(new KeyPressEventArgs((char) Keys.Enter));
        }

        private void GetThresholds()
        {
            txtDifferenceMarker.Text = _thresholds.DifferenceMarkers.ToString(CultureInfo.InvariantCulture);
            txtDifferenceResults.Text = _thresholds.DifferenceResults.ToString(CultureInfo.InvariantCulture);
            txtSensitivityMarker.Text = _thresholds.SensitivityMarkers.ToString(CultureInfo.InvariantCulture);
            txtSensitivityResults.Text = _thresholds.SensitivityResults.ToString(CultureInfo.InvariantCulture);
            txtSpecificityMarker.Text = _thresholds.SpecificityMarkers.ToString(CultureInfo.InvariantCulture);
            txtSpecificityResults.Text = _thresholds.SpecificityResults.ToString(CultureInfo.InvariantCulture);
        }

        private void SetThresholds()
        {
            _thresholds.DifferenceMarkers = double.Parse(txtDifferenceMarker.Text);
            _thresholds.DifferenceResults = double.Parse(txtDifferenceResults.Text);
            _thresholds.SensitivityMarkers = double.Parse(txtSensitivityMarker.Text);
            _thresholds.SensitivityResults = double.Parse(txtSensitivityResults.Text);
            _thresholds.SpecificityMarkers = double.Parse(txtSpecificityMarker.Text);
            _thresholds.SpecificityResults = double.Parse(txtSpecificityResults.Text);
        }

        /// <summary>Update the status bar label.</summary>
        private void UpdateStatus()
        {
            tslStatus.Text =
                string.Format(
                              "Max. Compound Marker Size: {0}  Marker Thresholds (%diff, sens., spec.): {1}, {2}, {3}  Results Thresholds (%diff, sens., spec.): {4}, {5}, {6}\nCompound Markers Shown: {7}",
                              txtMaxSize.Text,
                              _thresholds.DifferenceMarkers,
                              _thresholds.SensitivityMarkers,
                              _thresholds.SpecificityMarkers,
                              _thresholds.DifferenceResults,
                              _thresholds.SensitivityResults,
                              _thresholds.SpecificityResults,
                              _olv.Items.Count);
        }

        /// <summary>Initialize progress bar with a maximum value.</summary>
        /// <param name="iMax">New ProgressBar.Maximum value.</param>
        private void InitProgressBar(int iMax)
        {
            ProgressBar1.Visible = true;
            ProgressBar1.Value = 0;
            ProgressBar1.Maximum = iMax;
            ProgressBar1.Step = 1;
        }

        /// <summary>Enable or disable controls of the form.</summary>
        /// <param name="bEnabled">True to enable controls; false to disable.</param>
        private void EnableControls(bool bEnabled)
        {
            btnRun.Enabled =
                _olv.Enabled =
                tsbFile.Enabled =
                tsbPreferences.Enabled =
                ddbThresholds.Enabled =
                bEnabled;
        }

        private void KeyPressTxtThresholds(ToolStripTextBox textBox, KeyPressEventArgs e)
        {
            int i;
            if (int.TryParse(e.KeyChar.ToString(CultureInfo.InvariantCulture), out i) || e.KeyChar == '\b')
            {
                e.Handled = false;
            }
            else
                switch (e.KeyChar)
                {
                    case '.':
                        e.Handled = textBox.Text.Contains('.');
                        break;
                    case (char) Keys.Enter:
                        SetThresholds();
                        PopulateListview();
                        UpdateStatus();
                        break;
                    default:
                        e.Handled = true;
                        break;
                }
        }

        private void ValidateMaxSizeTextboxInput(KeyPressEventArgs e)
        {
            int i;
            if (int.TryParse(e.KeyChar.ToString(CultureInfo.InvariantCulture), out i) || e.KeyChar == '\b')
            {
                e.Handled = false;
            }
            else if (e.KeyChar == (char) Keys.Enter || e.KeyChar == '\t')
            {
                if (!(int.TryParse(txtMaxSize.Text, out _subsetSize)))
                {
                    MessageBox.Show("Invalid max. number of compound markers. Set to default (2).");
                    txtMaxSize.Text = "2";
                    _subsetSize = 2;
                }
                UpdateStatus();
            }
            else
            {
                e.Handled = true;
            }
        }

        #region Saving and exporting data

        private void SaveResults()
        {
            if (_olv.Items.Count == 0)
                return;

            string filename = string.Format("{0}_VS_{1}_CompoundMarkers_Results.txt", _fetGroup.GroupAName, _fetGroup.GroupBName);
            using (var sfd = new SaveFileDialog())
            {
                sfd.DefaultExt = ".txt";
                sfd.FileName = filename;
                if (sfd.ShowDialog() != DialogResult.OK)
                    return;

                filename = sfd.FileName;

                using (var writer = new StreamWriter(filename))
                {
                    var list = new List<string>();
                    foreach (ColumnHeader header in _olv.Columns)
                    {
                        list.Add(header.Text);
                    }
                    writer.WriteLine(string.Join("\t", list));
                    foreach (ListViewItem item in _olv.Items)
                    {
                        list.Clear();
                        foreach (ListViewItem.ListViewSubItem subItem in item.SubItems)
                        {
                            list.Add(subItem.Text);
                        }
                        writer.WriteLine(string.Join("\t", list));
                    }
                }
                tslStatus.Text = string.Format("Results written to {0}", filename);
                ProgressBar1.Visible = false;
            }
        }

        private void SaveResultsAndBinary()
        {
            if (_olv.Items.Count == 0)
                return;

            string filename = string.Format("{0}_VS_{1}_CompoundMarkers_Results_Binary", _fetGroup.GroupAName, _fetGroup.GroupBName);
            using (var sfd = new SaveFileDialog {AddExtension = true, DefaultExt = "txt", FileName = filename})
            {
                if (sfd.ShowDialog() != DialogResult.OK)
                    return;

                filename = sfd.FileName;

                using (var fs = new StreamWriter(filename))
                {
                    var list = new List<string>();
                    //first line = blank for information from listview and 1 for in group 1 and 0 for in group 2
                    for (int i = 0; i < _olv.Columns.Count; i++)
                    {
                        list.Add("");
                    }
                    //group 1 = 1
                    SingleMarker mbd = _singleMarkers[0];
                    for (int i = 0; i < mbd.GroupAStrainDistr.Length; i++)
                    {
                        list.Add("1");
                    }
                    //group 2 = 0
                    for (int i = 0; i < mbd.GroupBStrainDistr.Length; i++)
                    {
                        list.Add("0");
                    }
                    fs.WriteLine(string.Join("\t", list));
                    list.Clear();
                    //second line
                    for (int i = 0; i < _olv.Columns.Count; i++)
                    {
                        list.Add(_olv.Columns[i].Text);
                    }
                    //strain names
                    int[] strains1 = _fetGroup.GroupA.Strains;
                    int[] strains2 = _fetGroup.GroupB.Strains;
                    //group 1 strains
                    foreach (int strain in strains1)
                    {
                        list.Add(_analysis.Samples[strain]);
                    }
                    //group 2 strains
                    foreach (int strain in strains2)
                    {
                        list.Add(_analysis.Samples[strain]);
                    }

                    fs.WriteLine(string.Join("\t", list));

                    foreach (ListViewItem lvi in _olv.Items)
                    {
                        list.Clear();
                        foreach (ListViewItem.ListViewSubItem subItem in lvi.SubItems)
                        {
                            list.Add(subItem.Text);
                        }
                        //get binary distribution data for the marker
                        if (lvi.SubItems[1].Text == "1")
                        {
                            var m = (SingleMarker) lvi.Tag;
                            list.AddRange(m.GetBinaryDistr());
                        }
                        else
                        {
                            var m = (CompoundMarker) lvi.Tag;
                            list.AddRange(m.GetBinaryDistr());
                        }

                        fs.WriteLine(string.Join("\t", list));
                    }
                }
                tslStatus.Text = string.Format("Results and binary data written to {0}", filename);
            }
        }

        private void SaveBinary()
        {
            if (_olv.Items.Count == 0)
            {
                return;
            }
            string filename = string.Format("{0}_VS_{1}_CompoundMarkers_Binary", _fetGroup.GroupAName, _fetGroup.GroupAName);
            using (var sfd = new SaveFileDialog {AddExtension = true, DefaultExt = "txt", FileName = filename})
            {
                if (sfd.ShowDialog() != DialogResult.OK)
                    return;

                filename = sfd.FileName;
                using (var fs = new StreamWriter(filename))
                {
                    var list = new List<string> {""};
                    //first line = blank for first column and 1 for in group 1 and 0 for in group 2
                    //group 1 = 1
                    SingleMarker mbd = _singleMarkers[0];
                    for (int i = 0; i < mbd.GroupAStrainDistr.Length; i++)
                    {
                        list.Add("1");
                    }
                    //group 2 = 0
                    for (int i = 0; i < mbd.GroupBStrainDistr.Length; i++)
                    {
                        list.Add("0");
                    }
                    fs.WriteLine(string.Join("\t", list));
                    list.Clear();
                    //second line
                    list.Add(_olv.Columns[0].Text);

                    //strain names
                    int[] strains1 = _fetGroup.GroupA.Strains;
                    int[] strains2 = _fetGroup.GroupB.Strains;
                    //group 1 strains
                    foreach (int strain in strains1)
                    {
                        list.Add(_analysis.Samples[strain]);
                    }
                    //group 2 strains
                    foreach (int strain in strains2)
                    {
                        list.Add(_analysis.Samples[strain]);
                    }

                    fs.WriteLine(string.Join("\t", list));
                    list.Clear();
                    foreach (ListViewItem lvi in _olv.Items)
                    {
                        list.Clear();
                        list.Add(lvi.SubItems[0].Text);
                        //get binary distribution data for the marker
                        if (lvi.SubItems[1].Text == "1")
                        {
                            var m = (SingleMarker) lvi.Tag;
                            list.AddRange(m.GetBinaryDistr());
                        }
                        else
                        {
                            var m = (CompoundMarker) lvi.Tag;
                            list.AddRange(m.GetBinaryDistr());
                        }
                        fs.WriteLine(string.Join("\t", list));
                    }
                    tslStatus.Text = string.Format("Binary data written to {0}", filename);
                }
            }
        }

        #endregion

        #region SingleMarker object creation

        private void GetSingleMarkers(double pValueThreshold)
        {
            double threshold = _fetGroup.Threshold;
            int[] grpA = _fetGroup.GroupA.Strains;
            int[] grpB = _fetGroup.GroupB.Strains;

            int count = 0;
            foreach (ContingencyTable ct in _fetGroup.ContingencyTables)
            {
                double pVal = ct.GetFisher2TailPermutationTest();
                if (pVal <= pValueThreshold)
                {
                    int i = ct.GeneIndex;
                    var m = new SingleMarker(i,
                                             threshold,
                                             GetDataForStrainsOfMarker(i, grpA),
                                             GetDataForStrainsOfMarker(i, grpB),
                                             ct);
                    _singleMarkers.Add(m);
                    _markerIndices.Add(count++);
                }
            }
        }

        /// <summary>Get the raw data for the marker for the specified strains.</summary>
        /// <param name="index">Index of the marker.</param>
        /// <param name="strainIndices">Indices of strains.</param>
        /// <returns>Raw numerical data for the marker for the specified strains.</returns>
        private double[] GetDataForStrainsOfMarker(int index, int[] strainIndices)
        {
            var values = new double[strainIndices.Length];
            for (int i = 0; i < strainIndices.Length; i++)
            {
                values[i] = _analysis.RawData[index][strainIndices[i]];
            }
            return values;
        }

        #endregion

        #region Compound Marker Creation

        /// <summary>When the "Run" button is clicked, prompt user if he or she wants to create compound groups with the selected parameters.</summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnRunClick(object sender, EventArgs e)
        {
            int numCombos;
            string sizes;
            GetNumCompoundMarkers(out numCombos, out sizes, _markerIndices.Count, _subsetSize);
            if (MessageBox.Show(string.Format("Number of markers: {0}\nSubset Sizes: {1}\nNumber of Compound Markers: {2}\nAre you sure you want to continue?",
                                              _markerIndices.Count,
                                              string.Join(", ", sizes),
                                              numCombos),
                                "Continue?",
                                MessageBoxButtons.YesNo) == DialogResult.No)
                return;
            SetThresholds();
            InitProgressBar(numCombos);
            EnableControls(false);
            _bgw.RunWorkerAsync();
        }

        private static void GetNumCompoundMarkers(out int numCombos, out string sizes, int n, int k)
        {
            numCombos = 0;
            var values = new List<string>();
            while (k >= 2)
            {
                values.Add(k.ToString(CultureInfo.InvariantCulture));
                numCombos += CalcNumOfCombos(k, n);
                k--;
            }
            sizes = string.Join(",", values);
        }

        /// <summary>Calculate the number of combinations possible for n number of </summary>
        /// <param name="k"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        private static int CalcNumOfCombos(int k, int n)
        {
            int nK = n - k;

            double result = (n / (double) (k * nK));

            while (n > 1)
            {
                n--;
                if (k > 1)
                    k--;
                if (nK > 1)
                    nK--;
                result *= (n / (double) (k * nK));
            }
            return (int) result;
        }

        #endregion

        #region Listview functions

        private void SetupListView()
        {
            _olv.Columns.Add(new OLVColumn {Text = "Marker(s)"});
            _olv.Columns.Add(new OLVColumn {Text = "#"});
            _olv.Columns.Add(new OLVColumn {Text = "Marker Type"});
            _olv.Columns.Add(new OLVColumn {Text = "Sensitivity", AspectToStringFormat = "{0:0.0000}"});
            _olv.Columns.Add(new OLVColumn {Text = "Specificity", AspectToStringFormat = "{0:0.0000}"});
            _olv.Columns.Add(new OLVColumn {Text = "Specificity2", AspectToStringFormat = "{0:0.0000}"});
            _olv.Columns.Add(new OLVColumn {Text = "p-Value", AspectToStringFormat = "{0:0.00E0}"});
            _olv.Columns.Add(new OLVColumn {Text = "%Positive in Group A", AspectToStringFormat = "{0:p}"});
            _olv.Columns.Add(new OLVColumn {Text = "%Positive in Group B", AspectToStringFormat = "{0:p}"});
            _olv.Columns.Add(new OLVColumn {Text = "%Positive Difference", AspectToStringFormat = "{0:p}"});
            _olv.Columns.Add(new OLVColumn {Text = "%Avg Improvement", AspectToStringFormat = "{0:p}"});
            _olv.Columns.Add(new OLVColumn {Text = "Present Group A"});
            _olv.Columns.Add(new OLVColumn {Text = "Absent Group A"});
            _olv.Columns.Add(new OLVColumn {Text = "Present Group B"});
            _olv.Columns.Add(new OLVColumn {Text = "Absent Group B"});

            _olvCompoundMarker.GetColumn("Marker(s)").AspectGetter = GetMarkerNames;
            _olvCompoundMarker.GetColumn("#").AspectGetter = cm => cm.MarkerCount;
            _olvCompoundMarker.GetColumn("Marker Type").AspectGetter = cm => cm.MarkerTypesString;
            _olvCompoundMarker.GetColumn("Sensitivity").AspectGetter = cm => cm.Sensitivity;
            _olvCompoundMarker.GetColumn("Specificity").AspectGetter = cm => cm.Specificity;
            _olvCompoundMarker.GetColumn("Specificity2").AspectGetter = cm => cm.Specificity2;
            _olvCompoundMarker.GetColumn("p-Value").AspectGetter = cm => cm.PValue;
            _olvCompoundMarker.GetColumn("%Positive in Group A").AspectGetter = cm => cm.RateAB;
            _olvCompoundMarker.GetColumn("%Positive in Group B").AspectGetter = cm => cm.RateCD;
            _olvCompoundMarker.GetColumn("%Positive Difference").AspectGetter = cm => cm.RateDifference;
            _olvCompoundMarker.GetColumn("%Avg Improvement").AspectGetter = cm => cm.AverageImprovementOverSingleMarkers;
            _olvCompoundMarker.GetColumn("Present Group A").AspectGetter = cm => cm.A;
            _olvCompoundMarker.GetColumn("Absent Group A").AspectGetter = cm => cm.B;
            _olvCompoundMarker.GetColumn("Present Group B").AspectGetter = cm => cm.C;
            _olvCompoundMarker.GetColumn("Absent Group B").AspectGetter = cm => cm.D;
        }

        private string GetMarkerNames(CompoundMarker compoundMarker)
        {
            var list = new List<string>();
            foreach (SingleMarker singleMarker in compoundMarker.SingleMarkers)
            {
                list.Add(_analysis.Genes[singleMarker.MarkerIndex]);
            }
            return string.Join(";", list);
        }

        /// <summary>Update and populate the listview with compound marker information and single marker information.</summary>
        private void PopulateListview()
        {
            _olv.SetObjects(_compoundMarkers.Markers);
        }

        #endregion
    }
}