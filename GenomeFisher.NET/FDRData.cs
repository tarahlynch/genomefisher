﻿namespace GenomeFisher.NET
{
    public class FDRData
    {
        public FDRData(double dPValue)
        {
            PValue = dPValue;
            PooledPValue = 0.0;
            FilteringPi = 0.0;
            RejectionAreaProb = 0.0;
            FDR = 0.0;
            QValue = 0.0;
        }

        public double PValue { get; set; }

        public double PooledPValue { get; set; }

        public double FilteringPi { get; set; }

        public double RejectionAreaProb { get; set; }

        public double FDR { get; set; }

        public double QValue { get; set; }
    }
}