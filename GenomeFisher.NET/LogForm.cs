﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

namespace GenomeFisher.NET
{
    public partial class LogForm : Form
    {
        string _log;
        public LogForm(Point p, string l = "")
        {
            InitializeComponent();
            Location = p;
            _log = l;
            txtLog.Text = _log;
        }

        public string UpdateLog(string message)
        {
            _log = string.Format( "[{0}] {1}\n{2}",DateTime.Now.ToLongTimeString(), message, _log);
            txtLog.Text = _log;
            return _log;
        }

        private void SaveAsToolStripMenuItemClick(object sender, EventArgs e)
        {
            SaveAs();
        }

        private void SaveAs()
        {
            using (var sfd = new SaveFileDialog())
            {
                sfd.Title = "Save log to file";
                sfd.DefaultExt = ".txt";
                sfd.FileName = "log.txt";
                sfd.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
                if (sfd.ShowDialog() != DialogResult.OK) return;
                string f = sfd.FileName;
                File.WriteAllText(f, _log);
            }
        }

        private void Clear()
        {
            _log = "";
            txtLog.Text = _log;
        }

        private void ClearToolStripMenuItemClick(object sender, EventArgs e)
        {
            Clear();
        }

        private void CloseToolStripMenuItemClick(object sender, EventArgs e)
        {
            Close();
        }
    }
}
