﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using BrightIdeasSoftware;

namespace GenomeFisher.NET
{
    public partial class GenomeFisherMainForm : Form
    {
        private readonly CompoundMarkerThresholds _cmt = new CompoundMarkerThresholds();
        private readonly List<ListViewItem> _groupSelection = new List<ListViewItem>();
        private readonly List<ListViewFF> _lvwCat = new List<ListViewFF>();
        private readonly List<GroupCollection> _selectedGroupCollections = new List<GroupCollection>();
        private readonly List<Group> _selectedGroups = new List<Group>();
        private GenomeFisherAnalysis _analysis;
        private FileInfo _analysisFile;
        private string _dataFilename;
        private string _logString = "";
        private LogForm _logForm;
        private TypedObjectListView<FETGroup> _olvFETGroups;
        private TypedObjectListView<ContingencyTable> _olvFETResults;
        private TypedObjectListView<GroupCollection> _olvGroupCollections;
        private TypedObjectListView<Group> _olvGroups;
        private double _pValueThreshold;
        private string _sampleInfoFilename;

        private TestType _testType;

        public GenomeFisherMainForm()
        {
            InitializeComponent();

            SetupFETGroupsOLV();
            SetupFETResultsOLV();
            SetupGroupsOLV();
            SetupGroupCollectionsOLV();

            Load += (sender, args) => InitGenomeFisher();
            FormClosing += (sender, args) => ClosingGenomeFisher(args);

            //Drag drop events for opening analysis or starting new analysis from data files
            DragOver += (sender, args) => OnDragOverOrEnter(args);
            DragEnter += (sender, args) => OnDragOverOrEnter(args);
            DragDrop += (sender, args) => OnFileDrop(args);

            txtMinSizeGroup.KeyPress += (sender, args) => ValidateMinGroupSizeInput(args);
            txtPvalueThreshold.KeyPress += (sender, args) => ValidatePValueThresholdInput(args);
            txtThreshold.KeyPress += (sender, args) => ValidateThresholdInput(args);

            txtPvalueThreshold.LostFocus += (sender, args) => ValidatePValueThresholdInput(new KeyPressEventArgs((char)Keys.Enter));
            txtMinSizeGroup.LostFocus += (sender, args) => ValidateMinGroupSizeInput(new KeyPressEventArgs((char)Keys.Enter));
            txtThreshold.LostFocus += (sender, args) => ValidateThresholdInput(new KeyPressEventArgs((char)Keys.Enter));

            btnShowSigFETOnly.Click += (sender, args) => { btnShowSigFETOnly.Checked = !btnShowSigFETOnly.Checked; };
            removeSelectedFETComparisonsToolStripMenuItem.Click += (sender, args) => RemoveSelectedFETGroups();

            //copy listview info to clipboard
            lvwGroupSelection.KeyDown += (sender, args) => Misc.OnListViewCopyToClipboard(args, lvwGroupSelection);

            //get the sample info and numerical data files
            sampleInfoFileToolStripMenuItem.Click += (sender, args) => GetFile(false);
            numericDataFileToolStripMenuItem.Click += (sender, args) => GetFile(true);

            exitToolStripMenuItem.Click += (sender, args) => Close();
            SetGroupCreationModificationEvents();

            SetFETCalculationEvents();

            bgw.DoWork += (sender, args) => CalculateFETStatsInBackground();
            bgw.RunWorkerCompleted += (sender, args) => OnCalculateStatsCompleted();

            calculateFDRToolStripMenuItem.Click += (sender, args) => ShowFDRForm();


            btnDetermineCompoundMarkers.Click += (sender, args) => DetermineCompoundMarkers();

            contextMenuFETGroups.Opening += (sender, args) => Misc.OpeningContextMenu(args, olvFETGroups.Items.Count, _analysis);
            contextMenuFETResults.Opening += (sender, args) => Misc.OpeningContextMenu(args, olvFETResults.Items.Count, _analysis);

            closeToolStripMenuItem.Click += (sender, args) =>
                                                {
                                                    DialogResult dr = PromptUserToSaveAnalysis();
                                                    if (dr == DialogResult.Cancel) return;
                                                    if (dr == DialogResult.Yes)
                                                        SaveAnalysisFile();
                                                    CloseWork();
                                                };

            btnOpenThresholdsFile.Click += (sender, args) => OpenThresholdsFile();
            btnOpenDataFiles.Click += (sender, args) => OpenSampleInfoAndNumericalDataFiles();
            btnShowStatusLog.Click += (sender, args) => ShowStatusLog();

            btnSaveAs.Click += (sender, args) => SaveAsAnalysisFile();
            btnSave.Click += (sender, args) => SaveAnalysisFile();
        }

        private void InitGenomeFisher()
        {
            AllowDrop = true;
            EnableControls(false);
            openToolStripMenuItem.Enabled = true;
            ShowLogForm();
            var splash = new GenomeFisherSplashForm();
            splash.Show();
            ReadSettingsINI();
        }

        private void ClosingGenomeFisher(FormClosingEventArgs e)
        {
            if (_analysis != null)
            {
                DialogResult dr = MessageBox.Show("Save work before quitting Genome Fisher .NET?",
                                                  "Save work before quitting?",
                                                  MessageBoxButtons.YesNoCancel);

                if (dr != DialogResult.Cancel)
                {
                    if (dr == DialogResult.Yes)
                    {
                        if (_analysisFile == null)
                        {
                            SaveAsAnalysisFile();
                        }
                        else
                        {
                            SaveAnalysisFile();
                        }
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
            WriteSettingsINI();
        }

        private void SetFETCalculationEvents()
        {
            btnCalcPairwiseComparisons.Click += (sender, args) => CalculateFETStatsInBackground(TestType.PairwiseSelected, true);
            btnCalcNotInGroupComparisons.Click += (sender, args) => CalculateFETStatsInBackground(TestType.VsNotSelected, true);
            btnCalcAllPairwiseComparisons.Click += (sender, args) => CalculateFETStatsInBackground(TestType.PairwiseAll, false);
            btnCalcAllNotInGroupComparisons.Click += (sender, args) => CalculateFETStatsInBackground(TestType.VsNotAll, false);
        }

        private void SetGroupCreationModificationEvents()
        {
            btnCreateNewGroupIntoNewGroupCollection.Click += (sender, args) => CreateGroupWithNewName(false);
            btnCreateNewGroup.Click += (sender, args) => CreateGroupWithNewName(true);
            btnChangeGroupCollectionName.Click += (sender, args) => ChangeGroupCollectionName();
            btnAddNewGroupCollection.Click += (sender, args) => AddNewGroupCollection();
            btnRemoveGroupCollection.Click += (sender, args) => RemoveGroupCollection();
            btnChangeGroupName.Click += (sender, args) => ChangeGroupName();
            btnRemovewGroup.Click += (sender, args) => RemoveGroupFromCurrentCollection();
            btnClearSelection.Click += (sender, args) => ClearSelection();
            btnAutoGroupCreation.Click += (sender, args) => AutoGroupCreation();
            btnCompoundGroupCreation.Click += (sender, args) => CompoundGroupCreation();
        }

        /// <summary>Create and setup Category listview controls.</summary>
        private void CreateCategoryControls()
        {
            List<Category> cat = _analysis.Categories;
            _lvwCat.Clear();

            int listviewHeight = splitContainer2.Panel1.Height - 22;

            foreach (Category c in cat)
            {
                var l = new ListViewFF();
                l.ItemSelectionChanged += LItemSelectionChanged;
                l.Parent = splitContainer2.Panel1;
                l.Size = new Size(150, listviewHeight);
                l.Columns.Add(c.Name, 95);
                l.Columns.Add("Counts", 50);
                //l.CheckBoxes = true;
                var lvList = new List<ListViewItem>();
                for (int i = 0; i < c.Types.Length; i++)
                {
                    var lvItem = new ListViewItem(new[] { c.Types[i], c.Counts[i].ToString(CultureInfo.InvariantCulture) }) { BackColor = Color.LightSkyBlue };
                    lvList.Add(lvItem);
                }
                l.Items.AddRange(lvList.ToArray());
                _lvwCat.Add(l);
            }

            int lengthCount = 10;
            foreach (ListViewFF l in _lvwCat)
            {
                var p = new Point(lengthCount, 0);
                l.Location = p;
                l.Show();
                lengthCount += 160;
            }
        }

        private void SplitContainer2Panel1SizeChanged(object sender, EventArgs e)
        {
            if (_analysis == null)
            {
                return;
            }
            int listviewHeight = splitContainer2.Panel1.Height - 22;
            foreach (ListViewFF t in _lvwCat)
            {
                t.Height = listviewHeight;
            }
        }

        private void CloseWork()
        {
            EnableControls(false);
            _analysisFile = null;
            _dataFilename = null;
            _sampleInfoFilename = null;
            txtThreshold.Text = "1";
            txtPvalueThreshold.Text = "0.05";
            txtMinSizeGroup.Text = "2";
            _selectedGroups.Clear();
            _selectedGroupCollections.Clear();

            if (_analysis != null)
            {
                _analysis = null;
                for (int i = _lvwCat.Count - 1; i > -1; i--)
                {
                    _lvwCat[i].Dispose();
                }
                _lvwCat.Clear();
                olvFETGroups.SetObjects(null);
                olvFETResults.SetObjects(null);
                gbxFET.Text = "FET Statistics";
                lvwGroupSelection.Items.Clear();
                olvGroups.SetObjects(null);
                olvGroupCollections.SetObjects(null);
                gbxFET.Text = "Fisher Exact Test Results";
                gbxGroupSelection.Text = "Group Item Selection";
            }
            openToolStripMenuItem.Enabled = true;
            ShowLogForm();
        }

        private void DetermineCompoundMarkers()
        {
            new CompoundMarkerForm(_analysis, _olvFETGroups.SelectedObject, _pValueThreshold, _cmt).Show();
        }

        private void ShowFDRForm()
        {
            foreach (FETGroup fetGroup in _olvFETGroups.SelectedObjects)
            {
                new FalseDiscoveryRateForm(fetGroup, _analysis, string.Format("{0} VS {1}", fetGroup.GroupAName, fetGroup.GroupBName)).Show();
            }
        }

        private void AboutToolStripMenuItemClick(object sender, EventArgs e)
        {
            var about = new AboutBoxForm();
            about.Show();
        }

        #region GroupCollection

        private void SetupGroupCollectionsOLV()
        {
            _olvGroupCollections = new TypedObjectListView<GroupCollection>(olvGroupCollections);
            olvGroupCollections.IncludeColumnHeadersInCopy = true;
            olvGroupCollections.FullRowSelect = true;
            olvGroupCollections.GridLines = true;
            olvGroupCollections.UseAlternatingBackColors = true;
            olvGroupCollections.OwnerDraw = true;

            olvGroupCollections.ContextMenuStrip = contextMenuGroupCollection;

            olvGroupCollections.Columns.Add(new OLVColumn { Text = "Name", });
            olvGroupCollections.Columns.Add(new OLVColumn { Text = "Count",  });

            _olvGroupCollections.GetColumn("Name").AspectGetter = groupCollection => groupCollection.Name;
            _olvGroupCollections.GetColumn("Count").AspectGetter = groupCollection => groupCollection.Groups.Count;

            olvGroupCollections.SelectionChanged += (sender, args) =>
                                                        {
                                                            if (olvGroupCollections.SelectedObjects.Count == 0)
                                                                _selectedGroupCollections.Clear();
                                                            else
                                                            {
                                                                _selectedGroupCollections.Clear();
                                                                foreach (GroupCollection groupCollection in _olvGroupCollections.SelectedObjects)
                                                                {
                                                                    _selectedGroupCollections.Add(groupCollection);
                                                                }
                                                            }
                                                            UpdateGroups();
                                                            UpdateFETGroups();
                                                        };
            olvGroupCollections.ItemSelectionChanged += (sender, args) =>
                                                            {
                                                                if (olvGroupCollections.SelectedObjects.Count == 0)
                                                                {
                                                                    olvGroups.SetObjects(null);
                                                                    olvFETGroups.SetObjects(null);
                                                                    olvFETResults.SetObjects(null);
                                                                    gbxFET.Text = "FET Statistics";
                                                                }
                                                            };
        }

        private void UpdateGroupCollection(List<GroupCollection> groupCollections)
        {
            olvGroupCollections.SetObjects(groupCollections);
        }

        private void ChangeGroupCollectionName()
        {
            string name = _selectedGroupCollections[0].Name;
            if (Misc.InputBox("Change Group Collection Name",
                              "What would you like to name the group collection?",
                              ref name) != DialogResult.OK) return;
            _selectedGroupCollections[0].Name = name;
            UpdateGroupCollection(_analysis.GroupCollections);
        }

        private void ChangeGroupCollectionName(int index)
        {
            string name = _analysis.GroupCollections[index].Name;
            if (Misc.InputBox("Change Group Collection Name",
                              "What would you like to name the group collection?",
                              ref name) != DialogResult.OK) return;
            _analysis.GroupCollections[index].Name = name;
            UpdateGroupCollection(_analysis.GroupCollections);
        }

        private void AddNewGroupCollection()
        {
            _analysis.GroupCollections.Add(new GroupCollection(_analysis));
            ChangeGroupCollectionName(_analysis.GroupCollections.Count - 1);
            UpdateGroupCollection(_analysis.GroupCollections);
        }

        private void RemoveGroupCollection()
        {
            if (MessageBox.Show(
                                string.Format(
                                              "Are you sure you want to remove the group collection '{0}' with {1} groups? Doing so will also remove all FET comparisons associated with this group collection.",
                                              _selectedGroupCollections[0].Name,
                                              _selectedGroupCollections[0].Groups.Count),
                                "Remove group collection?",
                                MessageBoxButtons.YesNo) != DialogResult.Yes) return;
            _analysis.RemoveGroupCollection(_selectedGroupCollections);
            UpdateGroupCollection(_analysis.GroupCollections);
        }

        #endregion  

        #region Groups

        private void SetupGroupsOLV()
        {
            _olvGroups = new TypedObjectListView<Group>(olvGroups);
            olvGroups.IncludeColumnHeadersInCopy = true;
            olvGroups.FullRowSelect = true;
            olvGroups.GridLines = true;
            olvGroups.UseAlternatingBackColors = true;
            olvGroups.OwnerDraw = true;

            olvGroups.ContextMenuStrip = contextMenuGroups;

            olvGroups.Columns.Add(new OLVColumn {Text = "Name", FillsFreeSpace = true, FreeSpaceProportion = 50});
            olvGroups.Columns.Add(new OLVColumn {Text = "Count", FillsFreeSpace = true, FreeSpaceProportion = 50});

            _olvGroups.GetColumn("Name").AspectGetter = group => group.Name;
            _olvGroups.GetColumn("Count").AspectGetter = group => group.Count;
        }

        private void UpdateGroups()
        {
            olvGroups.SetObjects(_selectedGroupCollections.Count == 0 ? null : _selectedGroupCollections[0].Groups);
        }
        #endregion

        #region FETResults

        private void SetupFETResultsOLV()
        {
            _olvFETResults = new TypedObjectListView<ContingencyTable>(olvFETResults);
            olvFETResults.IncludeColumnHeadersInCopy = true;
            olvFETResults.FullRowSelect = true;
            olvFETResults.GridLines = true;
            olvFETResults.UseAlternatingBackColors = true;
            olvFETResults.HeaderUsesThemes = false;
            olvFETResults.OwnerDraw = true;

            olvFETResults.FormatRow += (sender, args) =>
                                           {
                                               var contingencyTable = (ContingencyTable) args.Model;

                                               double pValue = contingencyTable.GetFisher2TailPermutationTest();

                                               args.Item.BackColor = pValue > _pValueThreshold
                                                                         ? ColorInterpolator.InterpolateBetween(Color.Red, Color.FromArgb(232,155,0), 1.0 - pValue)
                                                                         : ColorInterpolator.InterpolateBetween(Color.FromArgb(1,255,1),
                                                                                                                Color.FromArgb(199,241,6),
                                                                                                                pValue/_pValueThreshold);
                                           };

            olvFETResults.Columns.Add(new OLVColumn {Text = "ID", });
            olvFETResults.Columns.Add(new OLVColumn {Text = "p-Value", AspectToStringFormat = "{0:0.00E0}",});
            olvFETResults.Columns.Add(new OLVColumn {Text = "Most Extreme p-Value", AspectToStringFormat = "{0:0.00E0}"});
            olvFETResults.Columns.Add(new OLVColumn
                                          {
                                              Text = "Log10((p-value)/(Most Extreme p-value))",
                                              AspectToStringFormat = "{0:0.00}",
                                          });
            olvFETResults.Columns.Add(new OLVColumn {Text = "Present in Group 1", });
            olvFETResults.Columns.Add(new OLVColumn {Text = "Absent in Group 1", });
            olvFETResults.Columns.Add(new OLVColumn {Text = "Present in Group 2", });
            olvFETResults.Columns.Add(new OLVColumn {Text = "Absent in Group 2", });

            int i = 0;
            _olvFETResults.GetColumn(i++).AspectGetter = contingencyTable => _analysis.Genes[contingencyTable.GeneIndex];
            _olvFETResults.GetColumn(i++).AspectGetter = contingencyTable => contingencyTable.GetFisher2TailPermutationTest();
            _olvFETResults.GetColumn(i++).AspectGetter = contingencyTable => contingencyTable.FETGroup.MostExtremePValue;
            _olvFETResults.GetColumn(i++).AspectGetter = contingencyTable => Math.Log10(contingencyTable.GetFisher2TailPermutationTest() / contingencyTable.FETGroup.MostExtremePValue);
            _olvFETResults.GetColumn(i++).AspectGetter = contingencyTable => contingencyTable.GetA();
            _olvFETResults.GetColumn(i++).AspectGetter = contingencyTable => contingencyTable.GetB();
            _olvFETResults.GetColumn(i++).AspectGetter = contingencyTable => contingencyTable.GetC();
            _olvFETResults.GetColumn(i).AspectGetter = contingencyTable => contingencyTable.GetD();

            olvFETResults.ContextMenuStrip = contextMenuFETResults;
        }

        private static class ColorInterpolator
        {
            delegate byte ComponentSelector(Color color);
            static ComponentSelector _redSelector = color => color.R;
            static ComponentSelector _greenSelector = color => color.G;
            static ComponentSelector _blueSelector = color => color.B;

            public static Color InterpolateBetween(
                Color endPoint1,
                Color endPoint2,
                double lambda)
            {
                if (lambda < 0 || lambda > 1)
                {
                    throw new ArgumentOutOfRangeException("lambda");
                }
                Color color = Color.FromArgb(
                    InterpolateComponent(endPoint1, endPoint2, lambda, _redSelector),
                    InterpolateComponent(endPoint1, endPoint2, lambda, _greenSelector),
                    InterpolateComponent(endPoint1, endPoint2, lambda, _blueSelector)
                );

                return color;
            }

            static byte InterpolateComponent(
                Color endPoint1,
                Color endPoint2,
                double lambda,
                ComponentSelector selector)
            {
                return (byte)(selector(endPoint1)
                    + (selector(endPoint2) - selector(endPoint1)) * lambda);
            }
        }

        private void UpdateFETResults(FETGroup fetComparison)
        {
            var i = 4;
            olvFETResults.GetColumn(i++).Text = string.Format("Present in {0}", fetComparison.GroupAName);
            olvFETResults.GetColumn(i++).Text = string.Format("Absent in {0}", fetComparison.GroupAName);
            olvFETResults.GetColumn(i++).Text = string.Format("Present in {0}", fetComparison.GroupBName);
            olvFETResults.GetColumn(i++).Text = string.Format("Absent in {0}", fetComparison.GroupBName);
            gbxFET.Text = string.Format("FET Results for {0} VS {1}",
                                        fetComparison.GroupAName,
                                        fetComparison.GroupBName);
            olvFETResults.SetObjects(fetComparison.ContingencyTables);
        }
        #endregion

        #region FETGroups

        private void SetupFETGroupsOLV()
        {
            _olvFETGroups = new TypedObjectListView<FETGroup>(olvFETGroups);

            olvFETGroups.IncludeColumnHeadersInCopy = true;
            olvFETGroups.FullRowSelect = true;
            olvFETGroups.GridLines = true;
            olvFETGroups.UseAlternatingBackColors = true;
            olvFETGroups.CheckBoxes = false;
            olvFETGroups.OwnerDraw = true;


            olvFETGroups.SelectionChanged += FETGroupSelectionChanged;

            olvFETGroups.Columns.Add(new OLVColumn { Text = "Group 1", TextAlign = HorizontalAlignment.Left, });
            olvFETGroups.Columns.Add(new OLVColumn { Text = "# Group 1", Width = 30 });
            olvFETGroups.Columns.Add(new OLVColumn { Text = "Group 2",  });
            olvFETGroups.Columns.Add(new OLVColumn { Text = "# Group 2", Width = 30 });
            olvFETGroups.Columns.Add(new OLVColumn { Text = "Sig. p-Values" });
            olvFETGroups.Columns.Add(new OLVColumn
                                         {
                                             Text = "Most Extreme p-Value",
                                             AspectToStringFormat = "{0:0.00E0}",
                                         });
            olvFETGroups.Columns.Add(new OLVColumn
                                         {
                                             Text = "Min. p-Value", 
                                             AspectToStringFormat = "{0:0.00E0}", 
                                             
                                         });
            olvFETGroups.Columns.Add(new OLVColumn
                                         {
                                             Text = "Log10((Min p-value)/(Most Extreme p-value))",
                                             AspectToStringFormat = "{0:0.000}",
                                         });
            olvFETGroups.Columns.Add(new OLVColumn { Text = "Threshold For Numerical Data" });

            _olvFETGroups.GetColumn("Group 1").AspectGetter = rowObject => rowObject.GroupAName;
            _olvFETGroups.GetColumn("# Group 1").AspectGetter = rowObject => rowObject.GroupACount;
            _olvFETGroups.GetColumn("Group 2").AspectGetter = rowObject => rowObject.GroupBName;
            _olvFETGroups.GetColumn("# Group 2").AspectGetter = rowObject => rowObject.GroupBCount;
            _olvFETGroups.GetColumn("Sig. p-Values").AspectGetter = rowObject =>
                                                                        {
                                                                            _analysis.PValueThreshold =
                                                                                double.Parse(txtPvalueThreshold.Text);
                                                                            rowObject.PValueThreshold =
                                                                                _analysis.PValueThreshold;
                                                                            return rowObject.CountSignificant;
                                                                        };
            _olvFETGroups.GetColumn("Most Extreme p-Value").AspectGetter = rowObject => rowObject.MostExtremePValue;
            _olvFETGroups.GetColumn("Min. p-Value").AspectGetter = rowObject => rowObject.MinPValue;
            _olvFETGroups.GetColumn("Log10((Min p-value)/(Most Extreme p-value))").AspectGetter =
                rowObject => Math.Log10((rowObject.MinPValue / rowObject.MostExtremePValue));
            _olvFETGroups.GetColumn("Threshold For Numerical Data").AspectGetter = rowObject => rowObject.Threshold;

            olvFETGroups.KeyDown += (sender, args) => OnFETGroupsKeyDown(args);

            olvFETGroups.ContextMenuStrip = contextMenuFETGroups;

            olvFETGroups.ItemSelectionChanged += (sender, args) =>
                                                     {
                                                         if (olvFETGroups.SelectedObjects.Count != 0) 
                                                             return;
                                                         olvFETResults.SetObjects(null);
                                                         gbxFET.Text = "FET Statistics";
                                                     };
        }

        private void UpdateFETGroups()
        {
            if (_selectedGroupCollections.Count == 0)
            {
                olvFETGroups.SetObjects(null);
                olvFETResults.SetObjects(null);
                gbxFET.Text = "FET Statistics";
            }
            else
            {
                var list = new List<FETGroup>();
                foreach (GroupCollection groupCollection in _selectedGroupCollections)
                {
                    foreach (FETGroup fetGroup in groupCollection.FETGroups)
                    {
                        list.Add(fetGroup);
                    }
                }
                olvFETGroups.SetObjects(list);
                olvFETGroups.RefreshObjects(list);
                olvFETResults.SetObjects(null);
                gbxFET.Text = "FET Statistics";
            }
        }

        private void FETGroupSelectionChanged(object sender, EventArgs args)
        {
            if (_olvFETGroups.SelectedObjects.Count == 0)
            {
                olvFETResults.SetObjects(null);
                gbxFET.Text = "FET Statistics";
            }
            else if (_olvFETGroups.SelectedObjects.Count > 0)
            {
                FETGroup fetGroup = _olvFETGroups.SelectedObjects[0];
                UpdateFETResults(fetGroup);
            }
        }
        private void OnFETGroupsKeyDown(KeyEventArgs args)
        {
            if (args.KeyCode == Keys.Delete)
                RemoveSelectedFETGroups();
            if (args.Control && args.KeyCode == Keys.R)
                RemoveSelectedFETGroups();
        }

        private void RemoveSelectedFETGroups()
        {
            if (_analysis == null)
                return;
            if (olvFETGroups.SelectedObjects.Count == 0)
                return;
            if (olvFETGroups.SelectedObjects.Count == 1)
            {
                FETGroup fetGroup = _olvFETGroups.SelectedObject;
                if (MessageBox.Show(string.Format("Remove FET Statistics '{0} vs {1}'?",
                                                  fetGroup.GroupAName,
                                                  fetGroup.GroupBName),
                                    "Are you sure you want to remove these statistics?",
                                    MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    GroupCollection groupCollection = fetGroup.GroupCollection;
                    groupCollection.Remove(fetGroup);
                    UpdateFETGroups();
                }
            }
            else
            {
                if (
                    MessageBox.Show("Remove the selected FET statistics?",
                                    "Are you sure you want to remove these statistics?",
                                    MessageBoxButtons.YesNo) ==
                    DialogResult.Yes)
                {
                    foreach (FETGroup fetGroup in _olvFETGroups.SelectedObjects)
                    {
                        GroupCollection groupCollection = fetGroup.GroupCollection;
                        groupCollection.Remove(fetGroup);
                    }
                    UpdateFETGroups();
                }
            }
        }

        #endregion

        #region Analysis saving\loading

        private void SaveAnalysisFile()
        {
            if (_analysisFile == null)
            {
                SaveAsAnalysisFile();
            }
            else
            {
                _analysis.Write(_analysisFile.FullName);
                _logString = _logForm.UpdateLog(string.Format("Analysis saved to {0}", _analysisFile.FullName));
            }
        }

        private void SaveAsAnalysisFile()
        {
            if (_analysis == null) return;
            using (var sfd = new SaveFileDialog())
            {
                sfd.FileName = string.Format("GenomeFisher_{0}", DateTime.Now.ToString("dd_MM_yyyy"));
                sfd.AddExtension = true;
                sfd.DefaultExt = "gf";
                if (sfd.ShowDialog() != DialogResult.OK)
                    return;
                _analysis.Write(sfd.FileName);
                _analysisFile = new FileInfo(sfd.FileName);
                _logString = _logForm.UpdateLog(string.Format("Analysis saved to {0}", _analysisFile.FullName));
            }
        }

        private Exception TryOpeningAnalysisFile(string filename)
        {
            try
            {
                _analysis = new GenomeFisherAnalysis(filename);

                EnableControls(true);
                txtThreshold.Text = _analysis.Threshold.ToString(CultureInfo.InvariantCulture);
                txtMinSizeGroup.Text = _analysis.MinGroupSize.ToString(CultureInfo.InvariantCulture);
                txtPvalueThreshold.Text = _analysis.PValueThreshold.ToString(CultureInfo.InvariantCulture);
                UpdateStatus();
                CreateCategoryControls();
                PopulateAutoGroupCombobox();
                if (_analysis.GroupCollections.Count > 0)
                {
                    UpdateGroupCollection(_analysis.GroupCollections);
                }
            }
            catch (Exception ex)
            {
                return ex;
            }
            return null;
        }

        /// <summary>Prompt user if they want to save analysis before continuing. Returns Cancel, Yes or No.</summary>
        private DialogResult PromptUserToSaveAnalysis()
        {
            if (_analysis == null) return DialogResult.Cancel;
            DialogResult dr = MessageBox.Show("Would you like to save before continuing?",
                                              "Would you like to save your work before continuing?",
                                              MessageBoxButtons.YesNoCancel);
            return dr;
        }

        #endregion 

        #region File dropping 

        private static void OnDragOverOrEnter(DragEventArgs args)
        {
            if (args.Data.GetDataPresent(DataFormats.FileDrop)) args.Effect = DragDropEffects.Copy;
        }

        private void OnFileDrop(DragEventArgs args)
        {
            if (!args.Data.GetDataPresent(DataFormats.FileDrop)) return;
            var files = (string[]) args.Data.GetData(DataFormats.FileDrop);
            if (files.Length == 1)
            {
                var fileinfo = new FileInfo(files[0]);
                if (fileinfo.Extension == ".gf")
                {
                    Exception ex = TryOpeningAnalysisFile(fileinfo.FullName);
                    if (ex != null)
                    {
                        _logString = _logForm.UpdateLog(string.Format("Exception:\n{0}", ex.Message));
                    }
                }
                else
                {
                    _logString = _logForm.UpdateLog(string.Format("Attempting to open unrecognized file format ({0}) as GenomeFisher analysis.", fileinfo.Extension));
                    Exception ex = TryOpeningAnalysisFile(fileinfo.FullName);
                    if (ex != null)
                    {
                        _logString = _logForm.UpdateLog(string.Format("Exception:\n{0}", ex.Message));
                    }
                }
            }
            if (files.Length == 2)
            {
                GetFiles(files);
            }
        }

        #endregion

        #region INI settings

        private void WriteSettingsINI()
        {
            var fi = new FileInfo(Application.ExecutablePath);
            if (fi.DirectoryName == null) return;
            string sSettingsPath = Path.Combine(fi.DirectoryName, "GenomeFisher_Settings.ini");
            using (var fs = new StreamWriter(sSettingsPath))
            {
                fs.WriteLine(btnShowSigFETOnly.Checked ? "1" : "0");
                fs.WriteLine(txtThreshold.Text);
                fs.WriteLine(txtMinSizeGroup.Text);
                fs.WriteLine(txtPvalueThreshold.Text);
                fs.Close();
            }
        }

        private void ReadSettingsINI()
        {
            var fi = new FileInfo(Application.ExecutablePath);
            if (fi.DirectoryName == null) return;
            string sSettingsPath = Path.Combine(fi.DirectoryName, "GenomeFisher_Settings.ini");
            if (!File.Exists(sSettingsPath))
            {
                return;
            }
            using (var fs = new StreamReader(sSettingsPath))
            {
                btnShowSigFETOnly.Checked = (fs.ReadLine() == "1");
                txtThreshold.Text = fs.ReadLine();
                txtMinSizeGroup.Text = fs.ReadLine();
                txtPvalueThreshold.Text = fs.ReadLine();
                string pvalueThreshold = txtPvalueThreshold.Text;
                if (pvalueThreshold != null) _pValueThreshold = double.Parse(pvalueThreshold);
                if (_analysis != null)
                {
                    UpdateStatus();
                }
                fs.Close();
            }
        }

        #endregion 

        #region Settings
        private void ValidateThresholdInput(KeyPressEventArgs e)
        {
            switch (e.KeyChar)
            {
                case '-':
                    e.Handled = (txtThreshold.Text.Contains('-'));
                    break;
                case '.':
                    e.Handled = (txtThreshold.Text.Contains('.'));
                    break;
                default:
                    int i;
                    if (int.TryParse(e.KeyChar.ToString(CultureInfo.InvariantCulture), out i) || e.KeyChar == '\b')
                    {
                        e.Handled = false;
                    }
                    else if (e.KeyChar == (char)Keys.Enter || e.KeyChar == '\t')
                    {
                        double threshold;
                        if (double.TryParse(txtThreshold.Text, out threshold))
                        {
                            _analysis.Threshold = threshold;
                            _logString = _logForm.UpdateLog(string.Format("Numerical data threshold set to {0}.", threshold));
                            UpdateStatus();
                        }
                        else
                        {
                            txtThreshold.Text = threshold.ToString(CultureInfo.InvariantCulture);
                            _logString = _logForm.UpdateLog("Invalid threshold. Threshold set to default of 1.0.");
                        }
                    }
                    else
                    {
                        e.Handled = true;
                    }
                    break;
            }
        }

        private void ValidateMinGroupSizeInput(KeyPressEventArgs e)
        {
            int i;
            if (int.TryParse(e.KeyChar.ToString(CultureInfo.InvariantCulture), out i) || e.KeyChar == '\b')
            {
                e.Handled = false;
            }
            else if (e.KeyChar == '\t' || e.KeyChar == (char)Keys.Enter)
            {
                UpdateStatus();
            }
            else
            {
                e.Handled = true;
            }
        }

        private void ValidatePValueThresholdInput(KeyPressEventArgs e)
        {
            int i;
            if (e.KeyChar == '.')
            {
                e.Handled = (txtPvalueThreshold.Text.Contains('.'));
            }
            else if (int.TryParse(e.KeyChar.ToString(CultureInfo.InvariantCulture), out i) || e.KeyChar == '\b')
            {
                e.Handled = false;
            }
            else if (e.KeyChar == (char)Keys.Enter || e.KeyChar == '\t')
            {
                double pValueThreshold;
                if (double.TryParse(txtPvalueThreshold.Text, out pValueThreshold))
                {
                    _analysis.PValueThreshold = pValueThreshold;
                    _pValueThreshold = pValueThreshold;
                    _logString = _logForm.UpdateLog(string.Format("Threshold for p-value set to {0}.", pValueThreshold));
                    UpdateStatus();
                }
                else
                {
                    _logString = _logForm.UpdateLog("Invalid p-value threshold value.");
                    txtThreshold.Text = _pValueThreshold.ToString(CultureInfo.InvariantCulture);
                }
            }
            else
            {
                e.Handled = true;
            }
        }

        #endregion

        #region Opening raw data files

        /// <summary>Get either the numeric or sample information file. If both are selected then the program will read both files and make sure that the format is valid.</summary>
        /// <param name="isNumericFile">True if numeric data file; false if sample info file.</param>
        private void GetFile(bool isNumericFile)
        {
            using (var ofd = new OpenFileDialog())
            {
                ofd.Title = isNumericFile ? "Select the numeric data file" : "Select the sample information file";
                ofd.Multiselect = false;
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    if (isNumericFile)
                    {
                        _dataFilename = ofd.FileName;
                        _logString = _logForm.UpdateLog(string.Format("Numeric data file selected: {0}", _dataFilename));
                    }
                    else
                    {
                        _sampleInfoFilename = ofd.FileName;
                        _logString = _logForm.UpdateLog(string.Format("Sample information file selected: {0}", _sampleInfoFilename));
                    }
                }
            }
            OpenAndValidateFiles();
        }

        private bool GetFiles(string[] filenames)
        {
            if (filenames.Length != 2) return false;
            //look for "sample" or "info" or any combination of the two words in one of the filenames
            string f1 = new FileInfo(filenames[0]).Name;
            MatchCollection mc1 = Regex.Matches(f1, "sample|info");

            string f2 = new FileInfo(filenames[1]).Name;
            MatchCollection mc2 = Regex.Matches(f2, "sample|info");

            if (mc1.Count > mc2.Count)
            {
                _sampleInfoFilename = filenames[0];
                _dataFilename = filenames[1];
            }
            else
            {
                _sampleInfoFilename = filenames[1];
                _dataFilename = filenames[0];
            }
            if (File.Exists(_dataFilename) && File.Exists(_sampleInfoFilename))
            {
                return OpenAndValidateFiles();
            }
            return false;
        }

        private bool OpenAndValidateFiles()
        {
            _analysis = new GenomeFisherAnalysis(_dataFilename, _sampleInfoFilename);
            //_data.EventStatData += PbEventStatData;
            txtThreshold.Enabled = true;
            _analysis.Threshold = 1.0;
            UpdateStatus();
            if (_analysis.Read())
            {
                //set up and populate the listview controls corresponding to the categories in the sample info file
                EnableControls(true);
                CreateCategoryControls();
                cbxCategories.Enabled = true;
                PopulateAutoGroupCombobox();
                //_groupInfo = new GroupInformationForm(this, _data, _selectedGroupCollection);
                return true;
            }
            CloseWork();

            return false;
        }

        private void OpenSampleInfoAndNumericalDataFiles()
        {
            DialogResult dr = PromptUserToSaveAnalysis();
            if (dr == DialogResult.Cancel) return;
            if (dr == DialogResult.Yes) SaveAnalysisFile();
            using (var ofd = new OpenFileDialog())
            {
                ofd.Multiselect = true;
                ofd.Title = "Please select both the sample info and numerical data files";
                if (ofd.ShowDialog() != DialogResult.OK)
                    return;
                CloseWork();
                if (!GetFiles(ofd.FileNames))
                {
                    _logForm.UpdateLog("Please select both the sample info and numerical data files only.");
                }
            }
        }

        private void OpenThresholdsFile()
        {
            if (_analysis == null)
                return;

            using (var ofd = new OpenFileDialog())
            {
                ofd.Title = "Select the thresholds file.";
                ofd.Multiselect = false;
                if (ofd.ShowDialog() != DialogResult.OK)
                    return;
                _analysis.ReadThresholds(ofd.FileName);
                _logString = _logForm.UpdateLog(string.Format("Thresholds retrieved from '{0}'.", ofd.FileName));
            }
        }

        #endregion

        #region Group Creation
        private void ContextMenuGroupCollectionOpening(object sender, CancelEventArgs e)
        {
            e.Cancel = (_analysis == null);
            btnCalcPairwiseComparisons.Enabled = (olvGroups.Items.Count >= 2);
            btnCalcAllNotInGroupComparisons.Enabled = (olvGroups.Items.Count >= 1);
            btnRemoveGroupCollection.Enabled = btnChangeGroupCollectionName.Enabled = (olvGroupCollections.SelectedObjects.Count > 0);
        }

        /// <summary>Populate the group auto-generation combobox with the names of the categories.</summary>
        private void PopulateAutoGroupCombobox()
        {
            var ls = new List<string>();
            foreach (Category c in _analysis.Categories)
            {
                ls.Add(c.Name);
            }
            // ReSharper disable CoVariantArrayConversion
            cbxCategories.Items.AddRange(ls.ToArray());
            // ReSharper restore CoVariantArrayConversion
        }

        /// <summary>Update the items that have been selected from the category listviews.</summary>
        private void UpdateSelectionListview(int lvwIndex, int lviIndex, bool isSelected)
        {
            var tmp = new[]
                          {
                              _analysis.Categories[lvwIndex].Name,
                              _analysis.Categories[lvwIndex].Types[lviIndex],
                              _analysis.Categories[lvwIndex].Counts[lviIndex].ToString(CultureInfo.InvariantCulture)
                          };
            if (isSelected)
            {
                _groupSelection.Add(new ListViewItem(tmp));
            }
            else
            {
                bool isRemoved = true;
                while (isRemoved)
                {
                    isRemoved = false;
                    foreach (ListViewItem lvi in _groupSelection)
                    {
                        var lviSubItems = new List<string>();
                        for (int i = 0; i < lvi.SubItems.Count; i++)
                        {
                            lviSubItems.Add(lvi.SubItems[i].Text);
                        }
                        if (lviSubItems.SequenceEqual(tmp))
                        {
                            _groupSelection.Remove(lvi);
                            isRemoved = true;
                            break;
                        }
                    }
                }
            }
            lvwGroupSelection.Items.Clear();
            if (_groupSelection.Count > 0)
            {
                lvwGroupSelection.Items.AddRange(_groupSelection.ToArray());
                Group g = CreateGroupFromSelection();
                gbxGroupSelection.Text = string.Format("Group Item Selection - {0} Strains In Selection",
                                                       g.Strains.Length);
            }
            else
            {
                gbxGroupSelection.Text = "Group Item Selection";
            }
        }

        private void CreateGroupWithNewName(bool addToCurrentGroupCollection)
        {
            if (lvwGroupSelection.Items.Count == 0)
                return;
            string groupName = GetDefaultGroupName();
            //allow the user to modify the default name
            if (Misc.InputBox("Specify name for this group", "Please specify a name for this group:", ref groupName) ==
                DialogResult.OK)
                CreateNewGroup(groupName, addToCurrentGroupCollection);
        }

        private Group CreateGroupFromSelection(string name = "")
        {
            //List of categories from categories list in GenomeFisherAnalysis
            List<Category> lcat = _analysis.Categories;
            //List for strain indices of different category subgroups
            var li = new List<int>();
            //List of category indices corresponding to selected category subgroups in li list
            var strains = new List<int>();
            for (int i = 0; i < _lvwCat.Count; i++)
            {
                foreach (int j in _lvwCat[i].SelectedIndices) //get the list of strain indices for each selected category subgroup
                    li = li.Concat(lcat[i].StrainGroup(j)).ToList();
                if (li.Count > 0)
                    strains = strains.Count == 0 ? strains.Concat(li).ToList() : strains.Intersect(li).ToList();
                li.Clear();
            }

            var g = new Group(strains.ToArray(), name);

            return g;
        }

        /// <summary>Create a new group and add the strains that are within the same category using OR logic. Strains in other categories are added using AND logic.</summary>
        /// <param name="name"></param>
        /// <param name="addToCurrentGroupCollection"> </param>
        private void CreateNewGroup(string name, bool addToCurrentGroupCollection)
        {
            Group g = CreateGroupFromSelection(name);
            //check if there is more than 0 strains in the selected group
            if (g.Strains.Length == 0)
                MessageBox.Show("No strains in selected group.");
            else
            {
                //add group to group list of current group collection or start a new group collection and add the group to that group list; update listview
                if (addToCurrentGroupCollection)
                    _selectedGroupCollections[0].Add(g);
                else
                    _analysis.CreateNewGroupCollection(g);
                UpdateGroupCollection(_analysis.GroupCollections);
                UpdateGroups();
            }
        }

        /// <summary>Get the default group name for the selected items from the category listviews.</summary>
        private string GetDefaultGroupName()
        {
            var s = new List<string>();

            string oldCat = "";
            var items = new List<string>();
            foreach (ListViewItem lvItem in lvwGroupSelection.Items)
            {
                string newCat = lvItem.SubItems[0].Text;

                if (oldCat == newCat)
                    items.Add(lvItem.SubItems[1].Text);
                else
                {
                    if (items.Count > 0)
                        s.Add(string.Join(",", items) + ")");
                    s.Add(newCat + "(");
                    items.Clear();
                    items.Add(lvItem.SubItems[1].Text);
                    oldCat = newCat;
                }
            }
            s.Add(string.Join(",", items) + ")");
            return string.Join("", s);
        }

        /// <summary>Clear the selected items from the category listviews.</summary>
        private void ClearSelection()
        {
            foreach (ListViewFF t in _lvwCat)
            {
                t.SelectedItems.Clear();
            }
            gbxGroupSelection.Text = "Group Item Selection";
        }

        private void TsbGroupCreationDropDownOpening(object sender, EventArgs e)
        {
            btnAutoGroupCreation.Enabled = btnCompoundGroupCreation.Enabled = (_analysis != null);
        }

        private void CbxCategoriesSelectedIndexChanged(object sender, EventArgs e)
        {
            int index = cbxCategories.SelectedIndex;
            if (
                MessageBox.Show(
                                "Auto-generate groups from category '" + cbxCategories.SelectedItem + "'? Minimum group size is " +
                                txtMinSizeGroup.Text,
                                "Auto-generate groups?",
                                MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                CreateGroupsFromCategory(index);
            }
            contextMenuGroups.Hide();
        }

        /// <summary>Auto-create groups for FET comparisons from a selected category.</summary>
        /// <param name="index">Index of the category selected by the user.</param>
        private void CreateGroupsFromCategory(int index)
        {
            int minGroupSize = int.Parse(txtMinSizeGroup.Text);
            _analysis.CreateNewGroupCollection(_analysis.Categories[index].Name);
            for (int i = 0; i < _analysis.Categories[index].Types.Length; i++)
            {
                if (_analysis.Categories[index].Counts[i] >= minGroupSize)
                {
                    _analysis.GroupCollections[_analysis.GroupCollections.Count - 1].Add(
                                                                                         new Group(_analysis.Categories[index].StrainGroup(i).ToArray(),
                                                                                                   string.Format("{0}_{1}",
                                                                                                                 _analysis.Categories[index].Name,
                                                                                                                 _analysis.Categories[index].Types[i])));
                }
            }
            UpdateGroupCollection(_analysis.GroupCollections);
            UpdateGroups();
        }

        private void AutoGroupCreation()
        {
            var selected = new List<int>();
            var cats = new List<string>();
            int count = 0;
            foreach (Category c in _analysis.Categories)
            {
                selected.Add(count++);
                cats.Add(c.Name);
            }

            if (Misc.ListviewSelect("Select Categories",
                                    "Select categories for group creation:",
                                    "Categories",
                                    cats,
                                    ref selected) != DialogResult.OK) return;

            foreach (int i in selected)
            {
                CreateGroupsFromCategory(i);
            }
        }

        private void CompoundGroupCreation()
        {
            var selected = new List<int>();
            var cats = new List<string>();
            foreach (Category c in _analysis.Categories)
            {
                cats.Add(c.Name);
            }

            if (Misc.ListviewSelect("Select Categories",
                                    "Select categories for group creation:",
                                    "Categories",
                                    cats,
                                    ref selected) != DialogResult.OK) return;
            CreateCompoundGroupsFromCategories(selected.ToArray());
        }

        private void CreateCompoundGroupsFromCategories(int[] indices)
        {
            int minGroupSize = int.Parse(txtMinSizeGroup.Text);

            var name = new List<string>();
            foreach (int i in indices)
            {
                name.Add(_analysis.Categories[i].Name);
            }

            _analysis.CreateNewGroupCollection(string.Join(", ", name));


            List<int> lAllPossibilites = GetAllCategorySubgroupPossibilities(indices);
            List<List<int>> lPermutations = GetCompoundGroupPermutations(lAllPossibilites.ToArray(), GetMaxPossibilities(lAllPossibilites));

            int gci = _analysis.GroupCollections.Count - 1;
            foreach (var permutation in lPermutations)
            {
                Group g = CreateCompoundGroup(indices, permutation.ToArray());
                if (g.Strains.Length >= minGroupSize)
                {
                    _analysis.GroupCollections[gci].Add(g);
                }
            }
            UpdateGroupCollection(_analysis.GroupCollections);
            UpdateGroups();
        }

        private List<int> GetAllCategorySubgroupPossibilities(int[] indices)
        {
            var tmp = new List<int>();

            for (int i = 0; i < indices.Length; i++)
            {
                int index = indices[i];
                tmp.Add(_analysis.Categories[index].Types.Length);
            }

            return tmp;
        }

        private static int GetMaxPossibilities(IEnumerable<int> p)
        {
            int i = 1;
            foreach (int j in p)
            {
                i *= j;
            }
            return i;
        }

        private static List<List<int>> GetCompoundGroupPermutations(int[] possibilities, int maxPossibilities)
        {
            var p = new List<List<int>>();
            int i = 0;
            int num = 0;
            int poss = GetPossibilities(possibilities, 1);
            while (i < maxPossibilities)
            {
                if (i > 0 && (i % poss) == 0)
                {
                    if (possibilities[0] == num + 1)
                    {
                        num = 0;
                    }
                    else
                    {
                        num++;
                    }
                }
                var il = new List<int> {num};
                p.Add(il);
                i++;
            }

            if (possibilities.Length > 1)
            {
                for (int z = 1; z < possibilities.Length; z++)
                {
                    i = 0;
                    poss = GetPossibilities(possibilities, z + 1);
                    num = 0;
                    while (i < maxPossibilities)
                    {
                        if (i > 0 && (i % poss) == 0)
                        {
                            if (possibilities[z] == num + 1)
                            {
                                num = 0;
                            }
                            else
                            {
                                num++;
                            }
                        }
                        p[i].Add(num);
                        i++;
                    }
                }
            }
            return p;
        }

        private static int GetPossibilities(int[] iPossibilities, int startIndex)
        {
            int j = 1;
            for (int i = startIndex; i < iPossibilities.Length; i++)
            {
                j *= iPossibilities[i];
            }
            return j;
        }

        private Group CreateCompoundGroup(int[] categories, int[] catItems)
        {
            //List of categories from categories list in GenomeFisherAnalysis
            List<Category> lcat = _analysis.Categories;
            //List for strain indices of different category subgroups
            var li = new List<int>();
            li.AddRange(lcat[categories[0]].StrainGroup(catItems[0]));
            for (int i = 1; i < categories.Length; i++)
            {
                li = li.Intersect(lcat[categories[i]].StrainGroup(catItems[i])).ToList();
                if (li.Count == 0)
                    break;
            }
            //start a new group
            Group g = li.Count > 0
                          ? new Group(li.ToArray(), GetCompoundGroupName(categories, catItems))
                          : new Group(li.ToArray(), "");
            return g;
        }

        private string GetCompoundGroupName(int[] categories, int[] catItems)
        {
            var name = new List<string>();
            for (int i = 0; i < categories.Length; i++)
            {
                name.Add(string.Format("{0}({1})",
                                       _analysis.Categories[categories[i]].Name,
                                       _analysis.Categories[categories[i]].Types[catItems[i]]));
            }
            return string.Join("_", name.ToArray());
        }

        /// <summary>Allow the user to change the name of the group with an input box.</summary>
        private void ChangeGroupName()
        {
            if (olvGroups.SelectedObjects.Count == 0)
                return;
            var group = (Group) olvGroups.SelectedObjects[0];
            string name = group.Name;
            if (Misc.InputBox("Change group name", "Change group name:", ref name) != DialogResult.OK)
                return;
            group.Name = name;
            UpdateGroups();
        }

        private void RemoveGroupFromCurrentCollection()
        {
            switch (olvGroups.SelectedObjects.Count)
            {
                case 0:
                    return;
                case 1:
                    {
                        var group = (Group) olvGroups.SelectedObjects[0];
                        if (MessageBox.Show(string.Format("Remove group '{0}'?",
                                                          group.Name),
                                            "Are you sure you want to remove this group?",
                                            MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            _selectedGroupCollections[0].Remove(group);
                        }
                    }
                    break;
                default:
                    if (MessageBox.Show("Remove selected groups?",
                                        "Are you sure you want to remove the selected groups?",
                                        MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        foreach (Group group in _olvGroups.SelectedObjects)
                        {
                            _selectedGroupCollections[0].Remove(group);
                        }
                    }
                    break;
            }
            UpdateGroupCollection(_analysis.GroupCollections);
            UpdateGroups();
        }

        private void ContextMenuGroupsOpening(object sender, CancelEventArgs e)
        {
            e.Cancel = (_analysis == null);
            btnChangeGroupName.Enabled = btnRemovewGroup.Enabled = (olvGroups.Items.Count > 0);
            btnCalcPairwiseComparisons.Enabled = (olvGroups.SelectedObjects.Count >= 2);
            btnCalcNotInGroupComparisons.Enabled = (olvGroups.SelectedObjects.Count >= 1);
        }

        private void LItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            if (e.IsSelected)
            {
                e.Item.BackColor = Color.DodgerBlue;
                e.Item.ForeColor = Color.White;
            }
            else
            {
                e.Item.BackColor = Color.LightSkyBlue;
                e.Item.ForeColor = Color.Black;
            }
            UpdateSelectionListview(_lvwCat.IndexOf((ListViewFF)e.Item.ListView), e.ItemIndex, e.IsSelected);
        }

        private void GetSelectedGroups()
        {
            _selectedGroups.Clear();
            foreach (Group group in _olvGroups.SelectedObjects)
            {
                _selectedGroups.Add(group);
            }
        }

        #endregion

        #region Statistic calculation
        private void CalculateFETStatsInBackground(TestType fet, bool onlySelected)
        {
            _testType = fet;
            _analysis.Threshold = double.Parse(txtThreshold.Text);
            _logString = _logForm.UpdateLog(string.Format("Numerical data threshold set at {0}", txtThreshold.Text));
            if (onlySelected)
                GetSelectedGroups();
            EnableControls(false);
            bgw.RunWorkerAsync();
        }

        private void CalculateFETStatsInBackground()
        {
            switch (_testType)
            {
                case TestType.PairwiseAll:
                    foreach (GroupCollection groupCollection in _selectedGroupCollections)
                    {
                        _analysis.CalcFisherStatsPairwiseAll(groupCollection);
                    }
                    break;
                case TestType.PairwiseSelected:
                    _analysis.CalcFisherStatsPairwiseAll(_selectedGroupCollections[0], _selectedGroups);
                    break;
                case TestType.VsNotAll:
                    foreach (GroupCollection groupCollection in _selectedGroupCollections)
                    {
                        _analysis.CalculateVsNotInGroup(groupCollection);
                    }
                    break;
                case TestType.VsNotSelected:
                    _analysis.CalculateVsNotInGroup(_selectedGroupCollections[0], _selectedGroups);
                    break;
            }
        }

        private void OnCalculateStatsCompleted()
        {
            _logString = _logForm.UpdateLog("Selected Fisher statistic calculations complete.");
            EnableControls(true);
            UpdateFETGroups();
        }

        private void EnableControls(bool enabled)
        {
            contextMenuGroups.Enabled =
                contextMenuFETResults.Enabled =
                contextMenuGroupSelection.Enabled =
                openToolStripMenuItem.Enabled =
                txtMinSizeGroup.Enabled =
                txtThreshold.Enabled =
                txtPvalueThreshold.Enabled =
                tsbGroupCreation.Enabled =
                btnCalcAllPairwiseComparisons.Enabled =
                cbxCategories.Enabled = enabled;
        }

        #endregion

        #region Status display/logging
        /// <summary>Show the status log window below the current main window.</summary>
        private void ShowLogForm()
        {
            if (_logForm == null || _logForm.IsDisposed)
            {
                Point p = Location;
                Size s = Size;
                p.Y += s.Height;
                _logForm = new LogForm(p, _logString);
            }
            _logForm.Show();
        }

        private void UpdateStatus()
        {
            if (_analysis == null)
                return;
            _analysis.MinGroupSize = int.Parse(txtMinSizeGroup.Text);
            _analysis.PValueThreshold = double.Parse(txtPvalueThreshold.Text);
            _analysis.Threshold = double.Parse(txtThreshold.Text);
            tslStatus.Text = string.Format("Threshold: {0}; Min. Auto-Group Size: {1}; p-Value Threshold: {2}",
                                           _analysis.Threshold,
                                           txtMinSizeGroup.Text,
                                           txtPvalueThreshold.Text);
        }

        private void ShowStatusLog()
        {
            if (_logForm.Visible)
            {
                btnShowStatusLog.Text = "Show Status Log Window";
                _logForm.Close();
            }
            else
            {
                btnShowStatusLog.Text = "Hide Status Log Window";
                ShowLogForm();
            }
        }

        /// <summary>When the Preferences toolstrip button is clicked, change the name of the show/hide status log window button to correspond to the open or closed status of the log window.</summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnPreferencesOpenShowHideLogForm(object sender, EventArgs e)
        {
            btnShowStatusLog.Text = _logForm.Visible ? "Hide Status Log Window" : "Show Status Log Window";
        }

        #endregion

        #region Nested type: TestType

        private enum TestType
        {
            PairwiseAll,
            PairwiseSelected,
            VsNotAll,
            VsNotSelected
        }

        #endregion
    }
}