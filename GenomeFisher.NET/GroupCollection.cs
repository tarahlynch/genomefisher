﻿using System.Collections.Generic;

namespace GenomeFisher.NET
{
    public class GroupCollection
    {
        private readonly GenomeFisherAnalysis _analysis;
        private readonly List<FETGroup> _fetGroups = new List<FETGroup>();
        private readonly List<Group> _groups = new List<Group>();

        public GroupCollection(GenomeFisherAnalysis analysis, string name = "GroupCollection")
        {
            _analysis = analysis;
            Name = name;
        }

        public GroupCollection(GenomeFisherAnalysis analysis, Group group, string name = "GroupCollection")
        {
            _analysis = analysis;
            Name = name;
            _groups.Add(group);
        }

        public GenomeFisherAnalysis Analysis { get { return _analysis; } }
        public string Name { get; set; }
        public List<FETGroup> FETGroups { get { return _fetGroups; } }

        public List<Group> Groups { get { return _groups; } }

        public void Add(Group g)
        {
            _groups.Add(g);
        }

        public void Add(FETGroup fetGroup)
        {
            _fetGroups.Add(fetGroup);
        }

        /// <summary>Remove a strain from a group in the group list.</summary>
        /// <param name="indexOfGroup">Index of the group in the group list.</param>
        /// <param name="indexOfStrain">Index of the strain in the specified group.</param>
        public void RemoveAt(int indexOfGroup, int indexOfStrain)
        {
            _groups[indexOfGroup].RemoveStrain(indexOfStrain);
        }

        public void Remove(Group group)
        {
            _groups.Remove(group);
        }

        public void Remove(FETGroup fetGroup)
        {
            _fetGroups.Remove(fetGroup);
        }
    }
}