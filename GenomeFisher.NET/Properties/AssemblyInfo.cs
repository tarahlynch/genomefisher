﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Genome Fisher .NET")]
[assembly: AssemblyDescription("Genome Fisher .NET is a statistical analysis tool using the Fisher Exact Test.\nProgramming:  Peter Kruczkiewicz\nDesign & Conception: Dr Eduardo Taboada\n\nPublic Health Agency of Canada (2011)")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Public Health Agency of Canada")]
[assembly: AssemblyProduct("GenomeFisher.NET")]
[assembly: AssemblyCopyright("Copyright © Public Health Agency of Canada 2011")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("db113d40-6d8e-40e9-a7ce-e758f56345f7")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
