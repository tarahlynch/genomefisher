﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using System.IO;

namespace GenomeFisher.NET
{
    public partial class GroupInformationForm : Form
    {
        readonly GenomeFisherAnalysis _data;
        readonly GenomeFisherMainForm _mainForm;
        public int GroupCollectionIndex;
        int _groupIndex;
        public GroupInformationForm(object sender, object sd, int indexGC)
        {
            GroupCollectionIndex = indexGC;
            InitializeComponent();
            _mainForm = sender as GenomeFisherMainForm;
            _data = sd as GenomeFisherAnalysis;
            SetupListview();
        }

        public void NewPosition(Point p)
        {
            Location = p;
        }

        private void SetupListview()
        {
            lvw1.Columns.Add("Sample");
            var columns = new List<ColumnHeader>();
            foreach (Category c in _data.Categories)
            {
                var ch = new ColumnHeader {Text = c.Name};
                columns.Add(ch);
            }
            lvw1.Columns.AddRange(columns.ToArray());
        }

        public void UpdateTest(int indexOfGroup, int indexOfGroupCollection)
        {
            _groupIndex = indexOfGroup;
            Text = string.Format("Group Information - {0}", _data.GroupCollections[indexOfGroupCollection].Groups[indexOfGroup].Name);
            var lvList = new List<ListViewItem>();
            foreach (int i in _data.GroupCollections[indexOfGroupCollection].Groups[indexOfGroup].Strains)
            {
                var tmp = new List<string> {_data.Samples[i]};
                foreach (Category c in _data.Categories)
                {
                    for (int j = 0; j < c.Types.Length; j++)
                    {
                        if (c.StrainGroup(j).Contains(i))
                        {
                            tmp.Add(c.Types[j]);
                            break;
                        }
                    }
                }
                var lvi = new ListViewItem(tmp.ToArray());
                lvList.Add(lvi);
            }
            lvw1.Items.Clear();
            lvw1.Items.AddRange(lvList.ToArray());
        }

        private void RemoveToolStripMenuItemClick(object sender, EventArgs e)
        {
            if (lvw1.SelectedIndices.Count == 0)
            {
                return;
            }
            if (lvw1.SelectedIndices.Count == 1)
            {
                if (MessageBox.Show(string.Format("Are you sure you want to remove '{0}?", lvw1.SelectedItems[0].Text), "Remove sample from group?", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    _data.GroupCollections[GroupCollectionIndex].RemoveAt(_groupIndex, lvw1.SelectedIndices[0]);
                    UpdateTest(_groupIndex, GroupCollectionIndex);
                    //_mainForm.UpdateGroupList();
                }
            }
            else
            {
                if (MessageBox.Show("Are you sure you want to remove the selected samples?", "Remove samples from group?", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    var tmp = new List<int>();
                    foreach (int i in lvw1.SelectedIndices)
                    {
                        tmp.Add(i);
                    }
                    tmp.Sort();
                    tmp.Reverse();
                    foreach (int i in tmp)
                    {
                        _data.GroupCollections[GroupCollectionIndex].RemoveAt(_groupIndex, i);
                    }
                    UpdateTest(_groupIndex, GroupCollectionIndex);
                    //_mainForm.UpdateGroupList();
                }
            }
        }

        private void SaveAsToolStripMenuItemClick(object sender, EventArgs e)
        {
            using (var sfd = new SaveFileDialog())
            {
                sfd.FileName = string.Format("{0}.txt", Text);
                sfd.Title = string.Format("Save {0}", Text);
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    WriteGroupInfo(sfd.FileName);
                }
            }
        }

        private void WriteGroupInfo(string filename)
        {
            var header = new List<string>();
            foreach (ColumnHeader ch in lvw1.Columns)
            {
                header.Add(ch.Text);
            }
            var fs = new StreamWriter(filename);
            fs.WriteLine(string.Join("\t", header));
            foreach (ListViewItem lvi in lvw1.Items)
            {
                var line = new List<string>();
                for (int i = 0; i < lvi.SubItems.Count; i++)
                {
                    line.Add(lvi.SubItems[i].Text);
                }
                fs.WriteLine(string.Join("\t", line));
            }
            fs.Close();
        }

    }
}
