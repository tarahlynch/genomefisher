﻿namespace GenomeFisher.NET
{
    partial class CompoundMarkerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CompoundMarkerForm));
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this._olv = new BrightIdeasSoftware.FastObjectListView();
            this.cm1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnOptimizeCompoundMarker = new System.Windows.Forms.ToolStripMenuItem();
            this.btnOptimizeExhaustive = new System.Windows.Forms.ToolStripMenuItem();
            this.btnDeconstruct = new System.Windows.Forms.ToolStripMenuItem();
            this.btnCreateCompoundMarker = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbFile = new System.Windows.Forms.ToolStripDropDownButton();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resultsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resultsBinaryDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.binaryDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsbPreferences = new System.Windows.Forms.ToolStripDropDownButton();
            this.maxCompoundMarkerSizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtMaxSize = new System.Windows.Forms.ToolStripTextBox();
            this.ddbThresholds = new System.Windows.Forms.ToolStripDropDownButton();
            this.markerThresholdsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sensitivityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtSensitivityMarker = new System.Windows.Forms.ToolStripTextBox();
            this.specificityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtSpecificityMarker = new System.Windows.Forms.ToolStripTextBox();
            this.differenceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtDifferenceMarker = new System.Windows.Forms.ToolStripTextBox();
            this.resultsThresholdsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sensitivityToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.txtSensitivityResults = new System.Windows.Forms.ToolStripTextBox();
            this.specificityToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.txtSpecificityResults = new System.Windows.Forms.ToolStripTextBox();
            this.differenceToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.txtDifferenceResults = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnFindBestCompoundMarker = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnRun = new System.Windows.Forms.ToolStripButton();
            this.ProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this._bgw = new System.ComponentModel.BackgroundWorker();
            this.btnCopyPatternToClipboard = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripContainer1.BottomToolStripPanel.SuspendLayout();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._olv)).BeginInit();
            this.cm1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.BottomToolStripPanel
            // 
            this.toolStripContainer1.BottomToolStripPanel.Controls.Add(this.statusStrip1);
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this._olv);
            this.toolStripContainer1.ContentPanel.Size = new System.Drawing.Size(920, 515);
            this.toolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.toolStripContainer1.Name = "toolStripContainer1";
            this.toolStripContainer1.Size = new System.Drawing.Size(920, 562);
            this.toolStripContainer1.TabIndex = 0;
            this.toolStripContainer1.Text = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.toolStrip1);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 0);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(920, 22);
            this.statusStrip1.TabIndex = 0;
            // 
            // tslStatus
            // 
            this.tslStatus.Name = "tslStatus";
            this.tslStatus.Size = new System.Drawing.Size(165, 17);
            this.tslStatus.Text = "Max. Compound Marker Size: ";
            this.tslStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _olv
            // 
            this._olv.CheckBoxes = false;
            this._olv.ContextMenuStrip = this.cm1;
            this._olv.Dock = System.Windows.Forms.DockStyle.Fill;
            this._olv.Location = new System.Drawing.Point(0, 0);
            this._olv.Name = "_olv";
            this._olv.ShowGroups = false;
            this._olv.Size = new System.Drawing.Size(920, 515);
            this._olv.TabIndex = 0;
            this._olv.UseCompatibleStateImageBehavior = false;
            this._olv.View = System.Windows.Forms.View.Details;
            this._olv.VirtualMode = true;
            // 
            // cm1
            // 
            this.cm1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnOptimizeCompoundMarker,
            this.btnOptimizeExhaustive,
            this.btnDeconstruct,
            this.btnCreateCompoundMarker,
            this.btnCopyPatternToClipboard});
            this.cm1.Name = "cm1";
            this.cm1.Size = new System.Drawing.Size(293, 136);
            // 
            // btnOptimizeCompoundMarker
            // 
            this.btnOptimizeCompoundMarker.Name = "btnOptimizeCompoundMarker";
            this.btnOptimizeCompoundMarker.Size = new System.Drawing.Size(292, 22);
            this.btnOptimizeCompoundMarker.Text = "Optimize Compound Marker";
            // 
            // btnOptimizeExhaustive
            // 
            this.btnOptimizeExhaustive.Name = "btnOptimizeExhaustive";
            this.btnOptimizeExhaustive.Size = new System.Drawing.Size(292, 22);
            this.btnOptimizeExhaustive.Text = "Optimize Compound Marker (exhaustive)";
            // 
            // btnDeconstruct
            // 
            this.btnDeconstruct.Name = "btnDeconstruct";
            this.btnDeconstruct.Size = new System.Drawing.Size(292, 22);
            this.btnDeconstruct.Text = "Deconstruct Compound Marker";
            // 
            // btnCreateCompoundMarker
            // 
            this.btnCreateCompoundMarker.Name = "btnCreateCompoundMarker";
            this.btnCreateCompoundMarker.Size = new System.Drawing.Size(292, 22);
            this.btnCreateCompoundMarker.Text = "Create Compound Marker From Selected";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbFile,
            this.tsbPreferences,
            this.ddbThresholds,
            this.toolStripSeparator2,
            this.btnFindBestCompoundMarker,
            this.toolStripSeparator1,
            this.btnRun,
            this.ProgressBar1});
            this.toolStrip1.Location = new System.Drawing.Point(3, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(512, 25);
            this.toolStrip1.TabIndex = 0;
            // 
            // tsbFile
            // 
            this.tsbFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.closeToolStripMenuItem});
            this.tsbFile.Image = ((System.Drawing.Image)(resources.GetObject("tsbFile.Image")));
            this.tsbFile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbFile.Name = "tsbFile";
            this.tsbFile.Size = new System.Drawing.Size(38, 22);
            this.tsbFile.Text = "File";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.resultsToolStripMenuItem,
            this.resultsBinaryDataToolStripMenuItem,
            this.binaryDataToolStripMenuItem});
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.saveToolStripMenuItem.Text = "Save...";
            // 
            // resultsToolStripMenuItem
            // 
            this.resultsToolStripMenuItem.Name = "resultsToolStripMenuItem";
            this.resultsToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.resultsToolStripMenuItem.Text = "Results";
            // 
            // resultsBinaryDataToolStripMenuItem
            // 
            this.resultsBinaryDataToolStripMenuItem.Name = "resultsBinaryDataToolStripMenuItem";
            this.resultsBinaryDataToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.resultsBinaryDataToolStripMenuItem.Text = "Results + Binary Data";
            // 
            // binaryDataToolStripMenuItem
            // 
            this.binaryDataToolStripMenuItem.Name = "binaryDataToolStripMenuItem";
            this.binaryDataToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.binaryDataToolStripMenuItem.Text = "Binary Data";
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.closeToolStripMenuItem.Text = "Close";
            // 
            // tsbPreferences
            // 
            this.tsbPreferences.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsbPreferences.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.maxCompoundMarkerSizeToolStripMenuItem,
            this.txtMaxSize});
            this.tsbPreferences.Image = ((System.Drawing.Image)(resources.GetObject("tsbPreferences.Image")));
            this.tsbPreferences.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbPreferences.Name = "tsbPreferences";
            this.tsbPreferences.Size = new System.Drawing.Size(81, 22);
            this.tsbPreferences.Text = "Preferences";
            // 
            // maxCompoundMarkerSizeToolStripMenuItem
            // 
            this.maxCompoundMarkerSizeToolStripMenuItem.Name = "maxCompoundMarkerSizeToolStripMenuItem";
            this.maxCompoundMarkerSizeToolStripMenuItem.Size = new System.Drawing.Size(226, 22);
            this.maxCompoundMarkerSizeToolStripMenuItem.Text = "Max. Compound Marker Size";
            // 
            // txtMaxSize
            // 
            this.txtMaxSize.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMaxSize.Name = "txtMaxSize";
            this.txtMaxSize.Size = new System.Drawing.Size(160, 23);
            this.txtMaxSize.Text = "2";
            this.txtMaxSize.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ddbThresholds
            // 
            this.ddbThresholds.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ddbThresholds.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.markerThresholdsToolStripMenuItem,
            this.resultsThresholdsToolStripMenuItem});
            this.ddbThresholds.Image = ((System.Drawing.Image)(resources.GetObject("ddbThresholds.Image")));
            this.ddbThresholds.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ddbThresholds.Name = "ddbThresholds";
            this.ddbThresholds.Size = new System.Drawing.Size(78, 22);
            this.ddbThresholds.Text = "Thresholds";
            // 
            // markerThresholdsToolStripMenuItem
            // 
            this.markerThresholdsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sensitivityToolStripMenuItem,
            this.specificityToolStripMenuItem,
            this.differenceToolStripMenuItem});
            this.markerThresholdsToolStripMenuItem.Name = "markerThresholdsToolStripMenuItem";
            this.markerThresholdsToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.markerThresholdsToolStripMenuItem.Text = "Marker Thresholds";
            // 
            // sensitivityToolStripMenuItem
            // 
            this.sensitivityToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txtSensitivityMarker});
            this.sensitivityToolStripMenuItem.Name = "sensitivityToolStripMenuItem";
            this.sensitivityToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.sensitivityToolStripMenuItem.Text = "Sensitivity";
            // 
            // txtSensitivityMarker
            // 
            this.txtSensitivityMarker.Name = "txtSensitivityMarker";
            this.txtSensitivityMarker.Size = new System.Drawing.Size(100, 23);
            this.txtSensitivityMarker.Text = "0.0";
            // 
            // specificityToolStripMenuItem
            // 
            this.specificityToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txtSpecificityMarker});
            this.specificityToolStripMenuItem.Name = "specificityToolStripMenuItem";
            this.specificityToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.specificityToolStripMenuItem.Text = "Specificity";
            // 
            // txtSpecificityMarker
            // 
            this.txtSpecificityMarker.Name = "txtSpecificityMarker";
            this.txtSpecificityMarker.Size = new System.Drawing.Size(100, 23);
            this.txtSpecificityMarker.Text = "0.0";
            // 
            // differenceToolStripMenuItem
            // 
            this.differenceToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txtDifferenceMarker});
            this.differenceToolStripMenuItem.Name = "differenceToolStripMenuItem";
            this.differenceToolStripMenuItem.Size = new System.Drawing.Size(138, 22);
            this.differenceToolStripMenuItem.Text = "%Difference";
            // 
            // txtDifferenceMarker
            // 
            this.txtDifferenceMarker.Name = "txtDifferenceMarker";
            this.txtDifferenceMarker.Size = new System.Drawing.Size(100, 23);
            this.txtDifferenceMarker.Text = "0.0";
            // 
            // resultsThresholdsToolStripMenuItem
            // 
            this.resultsThresholdsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sensitivityToolStripMenuItem1,
            this.specificityToolStripMenuItem1,
            this.differenceToolStripMenuItem1});
            this.resultsThresholdsToolStripMenuItem.Name = "resultsThresholdsToolStripMenuItem";
            this.resultsThresholdsToolStripMenuItem.Size = new System.Drawing.Size(172, 22);
            this.resultsThresholdsToolStripMenuItem.Text = "Results Thresholds";
            // 
            // sensitivityToolStripMenuItem1
            // 
            this.sensitivityToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txtSensitivityResults});
            this.sensitivityToolStripMenuItem1.Name = "sensitivityToolStripMenuItem1";
            this.sensitivityToolStripMenuItem1.Size = new System.Drawing.Size(138, 22);
            this.sensitivityToolStripMenuItem1.Text = "Sensitivity";
            // 
            // txtSensitivityResults
            // 
            this.txtSensitivityResults.Name = "txtSensitivityResults";
            this.txtSensitivityResults.Size = new System.Drawing.Size(100, 23);
            this.txtSensitivityResults.Text = "0.0";
            // 
            // specificityToolStripMenuItem1
            // 
            this.specificityToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txtSpecificityResults});
            this.specificityToolStripMenuItem1.Name = "specificityToolStripMenuItem1";
            this.specificityToolStripMenuItem1.Size = new System.Drawing.Size(138, 22);
            this.specificityToolStripMenuItem1.Text = "Specificity";
            // 
            // txtSpecificityResults
            // 
            this.txtSpecificityResults.Name = "txtSpecificityResults";
            this.txtSpecificityResults.Size = new System.Drawing.Size(100, 23);
            this.txtSpecificityResults.Text = "0.0";
            // 
            // differenceToolStripMenuItem1
            // 
            this.differenceToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txtDifferenceResults});
            this.differenceToolStripMenuItem1.Name = "differenceToolStripMenuItem1";
            this.differenceToolStripMenuItem1.Size = new System.Drawing.Size(138, 22);
            this.differenceToolStripMenuItem1.Text = "%Difference";
            // 
            // txtDifferenceResults
            // 
            this.txtDifferenceResults.Name = "txtDifferenceResults";
            this.txtDifferenceResults.Size = new System.Drawing.Size(100, 23);
            this.txtDifferenceResults.Text = "0.0";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btnFindBestCompoundMarker
            // 
            this.btnFindBestCompoundMarker.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnFindBestCompoundMarker.Image = ((System.Drawing.Image)(resources.GetObject("btnFindBestCompoundMarker.Image")));
            this.btnFindBestCompoundMarker.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnFindBestCompoundMarker.Name = "btnFindBestCompoundMarker";
            this.btnFindBestCompoundMarker.Size = new System.Drawing.Size(163, 22);
            this.btnFindBestCompoundMarker.Text = "Find Best Compound Marker";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btnRun
            // 
            this.btnRun.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnRun.Image = ((System.Drawing.Image)(resources.GetObject("btnRun.Image")));
            this.btnRun.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(128, 22);
            this.btnRun.Text = "Run Exhaustive Search";
            this.btnRun.Click += new System.EventHandler(this.BtnRunClick);
            // 
            // ProgressBar1
            // 
            this.ProgressBar1.Name = "ProgressBar1";
            this.ProgressBar1.Size = new System.Drawing.Size(100, 22);
            this.ProgressBar1.Visible = false;
            // 
            // _bgw
            // 
            this._bgw.WorkerReportsProgress = true;
            // 
            // btnCopyPatternToClipboard
            // 
            this.btnCopyPatternToClipboard.Name = "btnCopyPatternToClipboard";
            this.btnCopyPatternToClipboard.Size = new System.Drawing.Size(292, 22);
            this.btnCopyPatternToClipboard.Text = "Copy pattern to clipboard";
            // 
            // CompoundMarkerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(920, 562);
            this.Controls.Add(this.toolStripContainer1);
            this.Name = "CompoundMarkerForm";
            this.Text = "Compound Marker Determination";
            this.toolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.BottomToolStripPanel.PerformLayout();
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._olv)).EndInit();
            this.cm1.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripDropDownButton tsbFile;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripDropDownButton tsbPreferences;
        private System.Windows.Forms.ToolStripMenuItem maxCompoundMarkerSizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton btnRun;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslStatus;
        private System.Windows.Forms.ToolStripMenuItem resultsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resultsBinaryDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem binaryDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripProgressBar ProgressBar1;
        private System.ComponentModel.BackgroundWorker _bgw;
        private System.Windows.Forms.ToolStripDropDownButton ddbThresholds;
        private System.Windows.Forms.ToolStripMenuItem markerThresholdsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sensitivityToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox txtSensitivityMarker;
        private System.Windows.Forms.ToolStripMenuItem specificityToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox txtSpecificityMarker;
        private System.Windows.Forms.ToolStripMenuItem differenceToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox txtDifferenceMarker;
        private System.Windows.Forms.ToolStripMenuItem resultsThresholdsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sensitivityToolStripMenuItem1;
        private System.Windows.Forms.ToolStripTextBox txtSensitivityResults;
        private System.Windows.Forms.ToolStripMenuItem specificityToolStripMenuItem1;
        private System.Windows.Forms.ToolStripTextBox txtSpecificityResults;
        private System.Windows.Forms.ToolStripMenuItem differenceToolStripMenuItem1;
        private System.Windows.Forms.ToolStripTextBox txtDifferenceResults;
        private System.Windows.Forms.ToolStripButton btnFindBestCompoundMarker;
        private System.Windows.Forms.ContextMenuStrip cm1;
        private System.Windows.Forms.ToolStripMenuItem btnOptimizeCompoundMarker;
        private BrightIdeasSoftware.FastObjectListView _olv;
        private System.Windows.Forms.ToolStripTextBox txtMaxSize;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem btnOptimizeExhaustive;
        private System.Windows.Forms.ToolStripMenuItem btnDeconstruct;
        private System.Windows.Forms.ToolStripMenuItem btnCreateCompoundMarker;
        private System.Windows.Forms.ToolStripMenuItem btnCopyPatternToClipboard;
    }
}