﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;
using System.IO;

namespace GenomeFisher.NET
{
    public partial class FalseDiscoveryRateResultsForm : Form
    {
        readonly List<ContingencyTable> _ctList;
        readonly List<FDRData> _fdr;
        readonly GenomeFisherAnalysis _data;
        readonly string _name;


        public FalseDiscoveryRateResultsForm(GenomeFisherAnalysis genomeFisherAnalysis, FETGroup contingencyTables, IEnumerable<FDRData> fdrDataList, string comparisonName, bool mPIFiltering, bool bPositiveFDR)
        {
            _name = comparisonName;
            _data = genomeFisherAnalysis;
            var fet = contingencyTables;
            _ctList = fet.ContingencyTables;
            _fdr = fdrDataList as List<FDRData>;
            InitializeComponent();
            UpdateListview(mPIFiltering, bPositiveFDR);
            Load += (sender, args) =>  Text = string.Format("FDR Results for {0}", _name);
            lvw.KeyDown += (sender, args) => Misc.OnListViewCopyToClipboard(args, lvw);
        }

        private void UpdateListview(bool mPIFiltering, bool bPositiveFDR)
        {
            SetupColumns(mPIFiltering, bPositiveFDR);
            var lvl = new List<ListViewItem>();

            for (int i = 0; i < _ctList.Count; i++ )
            {
                ContingencyTable ct = _ctList[i];
                FDRData f = _fdr[i];
                var ldata = new List<string>
                                {
                                    _data.Genes[ct.GeneIndex],
                                    ct.GetA().ToString(CultureInfo.InvariantCulture),
                                    ct.GetB().ToString(CultureInfo.InvariantCulture),
                                    ct.GetC().ToString(CultureInfo.InvariantCulture),
                                    ct.GetD().ToString(CultureInfo.InvariantCulture),
                                    f.PValue.ToString("0.000e-0"),
                                    f.PooledPValue.ToString("0.000e-0")
                                };
                if (mPIFiltering)
                    ldata.Add(f.FilteringPi.ToString("0.000e-0"));
                if (bPositiveFDR)
                    ldata.Add(f.RejectionAreaProb.ToString("0.000e-0"));
                ldata.Add(f.FDR.ToString("0.000e-0"));
                ldata.Add(f.QValue.ToString("0.000e-0"));

                var lvi = new ListViewItem(ldata.ToArray());
                lvl.Add(lvi);
            }
            lvw.Items.AddRange(lvl.ToArray());
        }

        private void SetupColumns(bool mPIFiltering, bool bPositiveFDR)
        {
            string group2 = _name.Substring(_name.LastIndexOf(" VS ", StringComparison.Ordinal)+4);
            string group1 = _name.Remove(_name.LastIndexOf(" VS ", StringComparison.Ordinal));
            var tmp = new List<string>
                          {
                              "Genes",
                              "Present in " + group1,
                              "Absent in " + group1,
                              "Present in " + group2,
                              "Absent in " + group2,
                              "p-Value",
                              "Pooled p-Value"
                          };
            if (mPIFiltering)
                tmp.Add("Filtering PI");
            if (bPositiveFDR)
            {
                tmp.Add("Rejection Area Prob. (pr(R(p)>0))");
                tmp.Add("pFDR");
            }
            else
            {
                tmp.Add("FDR");
            }
            tmp.Add("q-Value");
            var lch = new List<ColumnHeader>();
            foreach (string s in tmp)
            {
                var ch = new ColumnHeader {Text = s};
                lch.Add(ch);
            }
            lvw.Columns.AddRange(lch.ToArray());
        }

        private void SaveToolStripMenuItemClick(object sender, EventArgs e)
        {
            Save();
        }

        private void Save()
        {
            if (lvw.Items.Count == 0)
            {
                return;
            }
            string filename = _name + "-FDR_Results.txt";
            using (var sfd = new SaveFileDialog())
            {
                sfd.Title = "Save FDR Data";
                sfd.DefaultExt = ".txt";
                sfd.FileName = filename;
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    filename = sfd.FileName;
                }
                else
                {
                    return;
                }
            }

            ListViewFF lv = lvw;
            var headers = new List<string>();
            var fs = new StreamWriter(filename);
            for (int i = 0; i < lv.Columns.Count; i++)
            {
                headers.Add(lv.Columns[i].Text);
            }
            fs.WriteLine(string.Join("\t", headers));
            foreach (ListViewItem lvi in lv.Items)
            {
                var line = new List<string>();
                for (int i = 0; i < lvi.SubItems.Count; i++)
                {
                    line.Add(lvi.SubItems[i].Text);
                }
                fs.WriteLine(string.Join("\t", line));
            }
            fs.Close();
        }

        private void CloseToolStripMenuItemClick(object sender, EventArgs e)
        {
            Close();
        }

        private void SaveAsToolStripMenuItemClick(object sender, EventArgs e)
        {
            Save();
        }

    }
}
