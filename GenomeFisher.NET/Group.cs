﻿using System.Collections.Generic;

namespace GenomeFisher.NET
{
    public class Group
    {
        /// <summary>List of strain index numbers for members belonging to group.</summary>
        private int[] _strainIndices;

        /// <summary>Constructor for Group class taking a list of strain index numbers and a group name as input.</summary>
        /// <param name="strains">List of strain index numbers.</param>
        /// <param name="name">Name of group.</param>
        public Group(int[] strains, string name)
        {
            _strainIndices = strains;
            Name = name;
        }

        /// <summary>Get list of strain index numbers.</summary>
        public int[] Strains { get { return _strainIndices; } }

        public int Count { get { return _strainIndices.Length; } }

        /// <summary>Get or set group name.</summary>
        public string Name { get; set; }

        /// <summary>Remove a strain from the group at a specified index.</summary>
        /// <param name="index">Index of strain to remove from group.</param>
        public void RemoveStrain(int index)
        {
            var tmp = new List<int>();
            tmp.AddRange(_strainIndices);
            tmp.RemoveAt(index);
            _strainIndices = tmp.ToArray();
        }
    }
}