﻿using System;
using System.Windows.Forms;

namespace GenomeFisher.NET
{
    public partial class FalseDiscoveryRateForm : Form
    {
        private readonly GenomeFisherAnalysis _data;
        private readonly FETGroup _fetGroup;
        private readonly string _name;

        public FalseDiscoveryRateForm(FETGroup fetGroup, GenomeFisherAnalysis statdata, string fetGroupString)
        {
            _name = fetGroupString;
            _data = statdata;
            _fetGroup = fetGroup;
            InitializeComponent();

            Load += (sender, args) =>
                        {
                            Text = string.Format("FDR - {0}", _name);
                            cmbPIMethod.SelectedIndex = 0;
                            MaximizeBox = false;
                        };
        }

        private void ChkSamplingCheckedChanged(object sender, EventArgs e)
        {
            bool bEnabled = chkSampling.Checked;
            chkAutomatedSampling.Enabled = bEnabled;
            lblSampleSize.Enabled = bEnabled;
            udSampleSize.Enabled = bEnabled;
        }

        private void BtnComputeClick(object sender, EventArgs e)
        {
            FalseDiscoveryRate.PiMethod piMethod = FalseDiscoveryRate.PiMethod.One;
            if (cmbPIMethod.SelectedIndex == 0)
                piMethod = FalseDiscoveryRate.PiMethod.One;
            else if (cmbPIMethod.SelectedIndex == 1)
                piMethod = FalseDiscoveryRate.PiMethod.WeightedSum;
            if (cmbPIMethod.SelectedIndex == 2)
                piMethod = FalseDiscoveryRate.PiMethod.DoubleAverage;
            if (chkFiltering.Checked)
                piMethod = FalseDiscoveryRate.PiMethod.Filtering;


            bool positiveFDR = chkPFDR.Checked;

            bool sampling = chkSampling.Checked;
            double convergenceEpsilon = 0.0;
            if (sampling)
            {
                bool automatedSampling = chkAutomatedSampling.Checked;
                if (automatedSampling)
                    convergenceEpsilon = 0.001;
            }

            var falseDiscoveryRate = new FalseDiscoveryRate(convergenceEpsilon, piMethod, positiveFDR, _fetGroup.ContingencyTables);
            falseDiscoveryRate.ComputeFDR();
            new FalseDiscoveryRateResultsForm(_data, _fetGroup, falseDiscoveryRate.FDRStats, _name, (piMethod == FalseDiscoveryRate.PiMethod.Filtering), positiveFDR).Show();
        }

        private void ChkFilteringCheckedChanged(object sender, EventArgs e)
        {
            lblPI.Enabled = !chkFiltering.Checked;
            cmbPIMethod.Enabled = !chkFiltering.Checked;
        }
 
    }
}
