﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Text;

namespace GenomeFisher.NET
{
    public class CompoundMarkerCollection
    {
        private readonly List<CompoundMarker> _compoundMarkers = new List<CompoundMarker>();
        //private readonly Dictionary<SingleMarker, int> _singleMarkerHash = new Dictionary<SingleMarker, int>(); 
        private readonly Dictionary<string, CompoundMarker> _compoundMarkerHash = new Dictionary<string, CompoundMarker>();   
        private readonly List<int> _markerIndices;

        private readonly List<SingleMarker> _singleMarkers;

        private int _subsetSize;

        public CompoundMarkerCollection(
            List<int> markerIndices,
            List<SingleMarker> singleMarkers,
            int subsetSize)
        {
            _markerIndices = markerIndices;
            _singleMarkers = singleMarkers;

            _subsetSize = subsetSize;

            foreach (SingleMarker marker in _singleMarkers)
            {
                _compoundMarkers.Add(new CompoundMarker(marker));
            }
        }

        public List<CompoundMarker> Markers { get { return _compoundMarkers; } }

        public void CreateCompoundMarker(List<CompoundMarker> compoundMarkers)
        {
            var hash = new HashSet<SingleMarker>();

            foreach (CompoundMarker compoundMarker in compoundMarkers)
            {
                foreach (SingleMarker singleMarker in compoundMarker.SingleMarkers)
                {
                    hash.Add(singleMarker);
                }
            }

            var list = new List<SingleMarker>();
            foreach (SingleMarker singleMarker in hash)
            {
                list.Add(singleMarker);
            }

            var cm = new CompoundMarker(list);
            _compoundMarkers.Add(cm);
        }

        public void OptimizeCompoundMarker(CompoundMarker cm, double minDiffChange)
        {
            var hash = new HashSet<SingleMarker>();

            foreach (SingleMarker m in cm.SingleMarkers)
            {
                hash.Add(m);
            }

            double previousDiff = double.Epsilon;

            while (true)
            {
                SingleMarker topMarker = null;
                double maxDiff = double.MinValue;
                foreach (SingleMarker sm in _singleMarkers)
                {
                    if (hash.Contains(sm))
                        continue;
                    var list = new List<SingleMarker> {sm};
                    foreach (SingleMarker singleMarker in hash)
                    {
                        list.Add(singleMarker);
                    }

                    cm = new CompoundMarker(list);

                    if (cm.RateDifference <= maxDiff)
                        continue;

                    maxDiff = cm.RateDifference;
                    topMarker = sm;
                }
                //break out if the new marker added doesn't add much more group resolution
                if (maxDiff - previousDiff < minDiffChange || topMarker == null)
                {
                    AddCompoundMarker(hash);
                    break;
                }

                previousDiff = maxDiff;
                hash.Add(topMarker);
            }
        }

        public void OptimizeCompoundMarkerExhaustive(CompoundMarker cm, double maxDiff, ref HashSet<string> setsTried )
        {
            var hash = new HashSet<SingleMarker>();

            Debug.Write("hash:");
            foreach (SingleMarker m in cm.SingleMarkers)
            {
                hash.Add(m);
                Debug.Write(string.Format("{0},", m.MarkerIndex));
            }
            Debug.WriteLine("|| {0} items", hash.Count);
            foreach (SingleMarker sm in _singleMarkers)
            {
                if (hash.Contains(sm))
                    continue;

                hash.Add(sm);
                var sb = new StringBuilder();
                foreach (SingleMarker singleMarker in _singleMarkers)
                {
                    sb.Append(hash.Contains(singleMarker) ? "1" : "0");
                }
                hash.Remove(sm);
                var binaryString = sb.ToString();

                //only test new sets that haven't been tried yet
                if (!setsTried.Add(binaryString))
                    continue;

                var list = new List<SingleMarker> { sm };
                foreach (SingleMarker singleMarker in hash)
                {
                    list.Add(singleMarker);
                }

                cm = new CompoundMarker(list);
                if (cm.RateDifference > maxDiff)
                {
                    maxDiff = cm.RateDifference;
                    OptimizeCompoundMarkerExhaustive(cm, maxDiff, ref setsTried);
                }
            }

            var sb2 = new StringBuilder();
            foreach (SingleMarker singleMarker in _singleMarkers)
            {
                sb2.Append(hash.Contains(singleMarker) ? "1" : "0");
            }
            var binaryString2 = sb2.ToString();
            if (_compoundMarkerHash.ContainsKey(binaryString2))
                return;

            Debug.WriteLine(binaryString2);
            Debug.Write("cm:");
            var list2 = new List<SingleMarker>();
            foreach (SingleMarker marker in hash)
            {
                list2.Add(marker);
                Debug.Write(string.Format(",{0}", marker.MarkerIndex));
            }
            Debug.WriteLine("|| {0} items!", list2.Count);
            cm = new CompoundMarker(list2);

            if (cm.MarkerCount == 1 )
                return;
            
            _compoundMarkers.Add(cm);
            _compoundMarkerHash.Add(binaryString2, cm);
        }

        private void AddCompoundMarker(HashSet<SingleMarker> markers)
        {
            var sb = new StringBuilder();
            foreach (SingleMarker singleMarker in _singleMarkers)
            {
                sb.Append(markers.Contains(singleMarker) ? "1" : "0");
            }
            var binaryString = sb.ToString();
            if (_compoundMarkerHash.ContainsKey(binaryString)) 
                return;
            var list = new List<SingleMarker>();
            foreach (SingleMarker marker in markers)
            {
                list.Add(marker);
            }
            var cm = new CompoundMarker(list);
            _compoundMarkers.Add(cm);
            _compoundMarkerHash.Add(binaryString, cm);
        }

        public void GetHighestPositiveRateDifferential(double minDiffChange)
        {
            var dict = new Dictionary<int, SingleMarker>();
            for (int i = 0; i < _markerIndices.Count; i++)
            {
                dict.Add(_markerIndices[i], _singleMarkers[i]);
            }
            var hash = new HashSet<int>();

            double maxDiff = double.MinValue;

            int n = _markerIndices.Count;
            int k = 3; /* The size of the subsets; for {1, 2}, {1, 3}, ... it's 2 */

            var comb = new int[k];
            for (int j = 0; j < k; j++)
            {
                //first combination
                comb[j] = j;
            }

            var lm = new List<SingleMarker>();
            foreach (int j in comb)
            {
                lm.Add(dict[j]);
            }
            var cm = new CompoundMarker(lm);

            double a = cm.ContingencyTable.GetA();
            double b = cm.ContingencyTable.GetB();
            double c = cm.ContingencyTable.GetC();
            double d = cm.ContingencyTable.GetD();

            double rateAB = (a / (a + b));
            double rateCD = (c / (c + d));


            double diff = Math.Abs(rateAB - rateCD);

            int index0 = 0;
            int index1 = 0;
            int index2 = 0;

            if (diff > maxDiff)
            {
                maxDiff = diff;
                index0 = comb[0];
                index1 = comb[1];
                index2 = comb[2];
            }
            while (NextCombination(ref comb, k, n))
            {
                lm = new List<SingleMarker>();
                foreach (int j in comb)
                {
                    lm.Add(_singleMarkers[_markerIndices[j]]);
                }
                cm = new CompoundMarker(lm);
                _compoundMarkers.Add(cm);

                a = cm.ContingencyTable.GetA();
                b = cm.ContingencyTable.GetB();
                c = cm.ContingencyTable.GetC();
                d = cm.ContingencyTable.GetD();

                rateAB = (a / (a + b));
                rateCD = (c / (c + d));

                diff = Math.Abs(rateAB - rateCD);

                if (diff > maxDiff)
                {
                    maxDiff = diff;
                    index0 = comb[0];
                    index1 = comb[1];
                    index2 = comb[2];
                }
            }

            hash.Add(index0);
            hash.Add(index1);
            hash.Add(index2);


            double previousDiff = maxDiff;
            while (true)
            {
                int markerIndex = -1;
                maxDiff = double.MinValue;
                foreach (int marker in _markerIndices)
                {
                    if (hash.Contains(marker))
                        continue;
                    var list = new List<SingleMarker> {dict[marker]};
                    foreach (int i in hash)
                    {
                        list.Add(dict[i]);
                    }

                    cm = new CompoundMarker(list);


                    if (cm.RateDifference > maxDiff)
                    {
                        maxDiff = diff;
                        markerIndex = marker;
                    }
                }
                //break out if the new marker added doesn't add much more specificity
                if (maxDiff - previousDiff < minDiffChange)
                {
                    var list = new List<SingleMarker>();
                    foreach (int i in hash)
                    {
                        list.Add(dict[i]);
                    }
                    cm = new CompoundMarker(list);
                    _compoundMarkers.Add(cm);
                    break;
                }

                previousDiff = maxDiff;
                hash.Add(markerIndex);
            }
        }

        public void CreateCompoundMarkers(BackgroundWorker bgw, int subsetSize, CompoundMarkerThresholds thresholds)
        {
            var sb = new StringBuilder();
            _subsetSize = subsetSize;
            int i = 2;
            do
            {
                int n = _markerIndices.Count; /* The size of the set; for {1, 2, 3, 4} it's 4 */
                int k = i; /* The size of the subsets; for {1, 2}, {1, 3}, ... it's 2 */

                var comb = new int[k];
                for (int j = 0; j < k; j++)
                {
                    //first combination
                    comb[j] = j;
                }

                var list = new List<SingleMarker>();
                foreach (int j in comb)
                {
                    list.Add(_singleMarkers[j]);
                }
                var cm = new CompoundMarker(list);
                if (cm.RateDifference >= thresholds.DifferenceResults &&
                    cm.Sensitivity >= thresholds.SensitivityResults &&
                    cm.Specificity >= thresholds.SpecificityResults)
                {
                    var hash = new HashSet<int>();
                    foreach (int index in comb)
                    {
                        hash.Add(index);
                    }
                    sb.Clear();

                    for (int j = 0; j < _singleMarkers.Count; j++)
                    {
                        sb.Append(hash.Contains(j) ? "1" : "0");
                    }
                    var binaryString = sb.ToString();
                    if (_compoundMarkerHash.ContainsKey(binaryString))
                        return;
                    _compoundMarkers.Add(cm);
                    _compoundMarkerHash.Add(binaryString, cm);
                }

                bgw.ReportProgress(0);
                //every other combination
                while (NextCombination(ref comb, k, n))
                {
                    list = new List<SingleMarker>();
                    foreach (int j in comb)
                    {
                        list.Add(_singleMarkers[j]);
                    }
                    cm = new CompoundMarker(list);
                    if (cm.RateDifference >= thresholds.DifferenceResults &&
                        cm.Sensitivity >= thresholds.SensitivityResults &&
                        cm.Specificity >= thresholds.SpecificityResults)
                    {
                        var hash = new HashSet<int>();
                        foreach (int index in comb)
                        {
                            hash.Add(index);
                        }
                        sb.Clear();

                        for (int j = 0; j < _singleMarkers.Count; j++)
                        {
                            sb.Append(hash.Contains(j) ? "1" : "0");
                        }
                        var binaryString = sb.ToString();
                        if (_compoundMarkerHash.ContainsKey(binaryString))
                            return;
                        _compoundMarkers.Add(cm);
                        _compoundMarkerHash.Add(binaryString, cm);
                    }
                    bgw.ReportProgress(0);
                }
                i++;
            } while (i <= _subsetSize);
        }

        private static bool NextCombination(ref int[] comb, int k, int n)
        {
            int i = k - 1;
            comb[i]++;
            while ((i > 0) && (comb[i] >= n - k + 1 + i))
            {
                i--;
                comb[i]++;
            }

            if (comb[0] > n - k) /* Combination (n-k, n-k+1, ..., n) reached */
                return false; /* No more combinations can be generated */

            /* comb now looks like (..., x, n, n, n, ..., n).
            Turn it into (..., x, x + 1, x + 2, ...) */
            for (i = i + 1; i < k; i++)
            {
                comb[i] = comb[i - 1] + 1;
            }
            return true;
        }
    }
}