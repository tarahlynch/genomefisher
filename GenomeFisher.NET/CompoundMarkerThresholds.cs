﻿namespace GenomeFisher.NET
{
    public class CompoundMarkerThresholds
    {
        public CompoundMarkerThresholds()
        {
            SpecificityMarkers = SpecificityResults =
                                 SensitivityMarkers = SensitivityResults =
                                                      DifferenceMarkers = DifferenceResults = 0;
        }

        public double SpecificityMarkers { get; set; }

        public double SensitivityMarkers { get; set; }

        public double DifferenceMarkers { get; set; }

        public double SpecificityResults { get; set; }

        public double SensitivityResults { get; set; }

        public double DifferenceResults { get; set; }
    }
}