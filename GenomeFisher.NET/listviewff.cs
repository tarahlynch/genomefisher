﻿using System.Windows.Forms;

namespace GenomeFisher.NET
{
    public class ListViewFF : ListView
    {
        public ListViewFF()
        {
            DoubleBuffered = true;
            View = View.Details;
            GridLines = true;
            FullRowSelect = true;
        }
    }
}
